Stripe.api_key = ENV['STRIPE_SECRET_KEY']

StripeEvent.setup do
  subscribe 'invoice.created' do |event|
    return if (event.livemode == true) ^ (Rails.env == 'production')
    # Define subscriber behavior based on the event object
    Rails.logger.info("Processing event #{event.type}")
    invoice = event.data.object
    if invoice.closed
      Rails.logger.info("Invoice #{invoice.id} is closed. Doing nothing.")
    else
      customer_id = invoice.customer
      user = User.where(stripe_customer_id: customer_id).first
      cost = user.calculate_payment_amount
      Stripe::InvoiceItem.create(
          customer: customer_id,
          amount: 100 * cost,
          currency: 'usd',
          description: "Payment for Slik.IO usage since #{user.find_last_payment_date()}"
      )
      user.last_payment_date = Time.now.utc()
      user.free_events = 0
      user.save!
    end
  end

  subscribe 'invoice.payment_succeeded' do |event|
    return if (event.livemode == true) ^ (Rails.env == 'production')
    Rails.logger.info("Processing event #{event.type}")
    invoice = event.data.object
    customer_id = invoice.customer
    user = User.where(stripe_customer_id: customer_id).first
    customer = Stripe::Customer.retrieve(customer_id)
    card = customer.cards.select { |c| c.id == customer.default_card }.first
    Analytics.track(
      user_id: user.email,
      event: 'transaction_succeeded',
      properties: {
        email: user.email,
        last_4_digits: card.last4,
        organization: card.name,
        address: card.address_line1,
        city: card.address_city,
        country: card.address_country,
        zip: card.address_zip,
        card_type: card.type,
        last_4_digits: card.last4,
        plan_name: 'default',
        amount: invoice.amount_due,
        currency: invoice.currency,
        transaction_date: Time.at(invoice.date).strftime('%B %d, %Y'),
        authorization_code: ''
      }
    )
  end

  subscribe 'invoice.payment_failed' do |event|
    return if (event.livemode == true) ^ (Rails.env == 'production')
    Rails.logger.info("Processing event #{event.type}")
    invoice = event.data.object
    customer_id = invoice.customer
    user = User.where(stripe_customer_id: customer_id).first
    Analytics.track(
      user_id: user.email,
      event: 'transaction_failed',
      properties:{
        email: user.email,
        name: "#{user.first_name} #{user.last_name}",
        transaction_date: Time.at(invoice.date).strftime('%B %d, %Y'),
        reason: ''
      }
    )
  end

  subscribe 'charge.refunded' do |event|
    return if (event.livemode == true) ^ (Rails.env == 'production')
    charge = event.data.object
    customer_id = charge.customer
    if customer_id
      amount = charge.amount_refunded
      user = User.where(stripe_customer_id: customer_id).first
      currency = charge.currency
      Analytics.track(
        user_id: user.email,
        event: 'refund',
        properties:{
          refund_date: Time.at(event.created).strftime('%B %d, %Y'),
          currency: currency,
          amount: amount
        }
      )
    else
      Rails.logger.info("Skipping event #{event.id}. Customer is nil.")
    end
  end

  #subscribe do |event|
  #  # Handle all event types - logging, etc.
  #end
end

#0.5: 342 000 - 350 000 ~ $10,000
#0.8: 580 000 -