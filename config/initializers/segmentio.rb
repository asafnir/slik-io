Analytics = AnalyticsRuby
Analytics.init(secret: ENV['SEGMENTIO_SECRET_KEY'], batch_size: 1)