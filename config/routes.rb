Visually::Application.routes.draw do
  authenticated :user do
    root :to => 'dashboard#index'
  end
  root :to => "home#index"
  
  devise_for :users, controllers: { registrations: 'devise_users/registrations', :sessions => "devise_users/sessions",
                                    :passwords => "devise_users/passwords", :confirmations => "devise_users/confirmations"}
  devise_scope :user do
    get 'users/email_available', to: 'devise_users/registrations#email_available'
    get 'users/current', to: 'devise_users/sessions#current'
    namespace :devise_users do
      resources :sessions, :only => [:create, :destroy]
    end
  end
  resources :users, only: [:index, :show, :destroy, :update]

  post 'collections/:id/load_csv' => 'collections#load_csv'

  get "api/v1/charts/dynamic" => 'api/v1/charts#dynamic'
  get "api/v1/charts/exists" => 'api/v1/charts#exists'
  #post "api/v1/charts" => 'api/v1/charts#create'

  namespace :api do
    namespace :v1 do
      resources 'charts', controller: 'charts'
      resources 'collections' do
        resources 'data', controller: 'data'
      end
    end
  end

  resources :accounts
  resources :uploads
  resources :collections

  put 'accounts/:id/cc' => 'accounts#updateCC'

  get "dashboard" => "dashboard#index"
  get "bonuses" => "bonuses#index"
  post "bonuses" => "bonuses#create"
  get "charts/:chart_id" => 'api/v1/charts#show'

  get "feedback" => "feedback#index"


  mount StripeEvent::Engine => '/stripe'

  #get '/*a' => redirect('/')
end