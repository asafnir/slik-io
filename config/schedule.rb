set :environment, ENV['RAILS_ENV']
puts "Configuring cron with #{@environment} settings"

job_type :rvmrunner, "cd :path && ~/.rvm/bin/app.slik.io_bundle exec rails runner -e :environment ':task' :output"

interval = @environment == 'production' ? 1.hour : 1.minute
every interval do
  rvmrunner "User.enforce_limit_on_free_tier_users"
end
