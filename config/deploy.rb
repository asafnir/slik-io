set :whenever_command, "~/.rvm/bin/app.slik.io_bundle exec whenever"
set :rails_env, proc { fetch :stage }

set :application, 'app.slik.io'
set :repo_url, 'git@bitbucket.org:shapigor/slik.git'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

set :deploy_to, '/u/apps/app.slik.io'
set :scm, :git

set :format, :pretty
set :log_level, :debug
# set :pty, true

set :linked_files, %w{config/mongoid.yml config/application.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/assets}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :keep_releases, 10
