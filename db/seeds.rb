# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) can be set in the file config/application.yml.
# See http://railsapps.github.io/rails-environment-variables.html
puts 'ROLES'
YAML.load(ENV['ROLES']).each do |role|
  Role.mongo_session['roles'].insert({ :name => role })
  puts 'role: ' << role
end
puts "Creating Admin user (#{ENV['ADMIN_EMAIL']})"
user = User.create! :name => ENV['ADMIN_NAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup
user.skip_confirmation!
user.save!
user.add_role :admin

puts "Creating Regular user (#{ENV['USER_EMAIL']})"
user = User.create! :name => ENV['USER_NAME'].dup, :email => ENV['USER_EMAIL'].dup, :password => ENV['USER_PASSWORD'].dup, :password_confirmation => ENV['USER_PASSWORD'].dup
user.skip_confirmation!
user.save!
user.add_role :user
["test1", "test2", "test3"].each {|c| user.add_collection(c) }
col = user.add_collection "Test"

puts "Creating public collections"
UserCollection.new({
    :name => 'CrunchBase data collection',
    :is_public => true
  }).save

public_col = UserCollection.new({
    :name => 'Public Demo Data',
    :is_public => true
  })
public_col.save

UserCollection.fill_with_random_data(col)
UserCollection.fill_with_random_data(public_col)

# Pie chart
chart1 = user.charts.new title: 'Pie chart (int by string)', is_draft: false
chart1.name = "Pie chart for test collection"
chart1.user_collection = col
chart1.public_id = "PIE_INT_BY_STR"
chart1.dimensions.build field_path: 'str', label: "String"
chart1.values.build field_path: 'int_val', aggregation_function: :avg, label: "Avg Int"
chart1.save!

# Line chart
chart2 = user.charts.new title: 'Line chart (int by date)', is_draft: false
chart2.name = "Line chart for test collection"
chart2.user_collection = col
chart2.public_id = "LINE_INT_BY_DATE"
chart2.dimensions.build field_path: 'date', label: 'Date'
chart2.values.build field_path: 'int_val', aggregation_function: :sum, label: "Sum Int"
chart2.save!

# Bar(column) chart
chart3 = user.charts.new title: 'Column chart (int by date)', is_draft: false
chart3.name = "Column chart for test collection"
chart3.user_collection = col
chart3.chart_type = :column
chart3.public_id = "COLUMN_INT_BY_DATE"
chart3.dimensions.build field_path: 'date', label: 'Event Date'
chart3.values.build field_path: 'int_val', aggregation_function: :avg, label: "Avg Int"
chart3.save!

# Multi-series bar chart
chart4 = user.charts.new title: "Multibar chart", is_draft: false
chart4.name = "MultiBar chart for test collection"
chart4.user_collection = col
chart4.chart_type = :column
chart4.public_id = "COLUMN_INT_AND_FLOAT_BY_DATE"
chart4.dimensions.build field_path: 'date', label: "Date"
chart4.values.build field_path: 'int_val', aggregation_function: :sum, label: 'Sum Int'
chart4.values.build field_path: 'float_val', aggregation_function: :sum, label: 'Sum Float'
chart4.save!

# Multi-series line chart
chart5 = user.charts.new title: "Multiline chart", is_draft: false
chart5.name = "MultiLine chart for test collection"
chart5.user_collection = col
chart5.chart_type = :line
chart5.public_id = "LINE_INT_AND_FLOAT_BY_DATE"
chart5.dimensions.build field_path: 'date', label: "Date"
chart5.values.build field_path: 'int_val', aggregation_function: :sum, label: 'Sum Int'
chart5.values.build field_path: 'float_val', aggregation_function: :sum, label: 'Sum Float'
chart5.save!

# Filtered chart
chart6 = user.charts.new title: "Filtered chart", is_draft: false
chart6.user_collection = col
chart6.chart_type = :line
chart6.filters = ["str == 'a'", "date >= '#{50000.minutes.ago.iso8601}'"]
chart6.dimensions.build field_path: 'date', label: "Date"
chart6.values.build field_path: 'int_val', aggregation_function: :sum, label: "Sum Int (where str = 'a')"
chart6.save!

col.charts = [chart1, chart2, chart3, chart4, chart5, chart6]
col.save!
