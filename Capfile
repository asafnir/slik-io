# Load DSL and Setup Up Stages
require 'capistrano/setup'

# Includes default deployment tasks
require 'capistrano/deploy'

# Includes tasks from other gems included in your Gemfile
#
# For documentation on these, see for example:
#
#   https://github.com/capistrano/rvm
#   https://github.com/capistrano/rbenv
#   https://github.com/capistrano/bundler
#   https://github.com/capistrano/rails/tree/master/assets
#   https://github.com/capistrano/rails/tree/master/migrations
#
require 'capistrano/rvm'
# require 'capistrano/rbenv'
require 'capistrano/bundler'
require 'capistrano/rails/assets'
# require 'capistrano/rails/migrations'

set     :rvm_map_bins, %w{rake gem bundle ruby}
set     :rvm_path, "~/.rvm"
set     :rvm_type, :user
set     :rvm_ruby, '1.9.3-p448'

# Loads custom tasks from `lib/capistrano/tasks' if you have any defined.
Dir.glob('lib/capistrano/tasks/*.cap').each { |r| import r }

namespace :whenever do
  desc 'Whenever update crontab'
  task :update_crontab do
    cmd = fetch(:whenever_command) || "bundle exec whenever"
    app_id = fetch :application
    env = fetch :rails_env
    on roles(:app), in: :sequence, wait: 10 do
      execute "cd #{release_path} && #{cmd} --update-crontab #{app_id} --set environment=#{env}"
    end
  end
end

namespace :deploy do
  task :print_vars do
    puts "Application: #{fetch :application}"
    puts "Stage: #{fetch :stage}"
    puts "Rails env: #{fetch :rails_env}"
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  # Because of the structure of our repository we need to create symlinks to the files in _src/web subfolder
  before 'deploy:symlink:shared', :subdir_symlinks do
    on roles(:app) do
      execute "for i in `ls #{release_path}/_src/web`; do ln -s #{release_path}/_src/web/$i #{release_path}/$i; done;"
    end
    # Dir["#{release_path}/*"].each {|f|
    #   puts f
    # }
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within latest_release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  # Capistrano 3 pre-prelease BUGFIX:
  # For some reason the symlinked assets folder is removed at the end of the deployment
  # This code symlinks it again
  before :restart, :assets_symlink do
    on roles(:app) do
      execute "ln -s #{shared_path}/public/assets #{release_path}/public/assets"
    end
  end

  before :starting, 'deploy:print_vars'
  after :finishing, 'deploy:cleanup'
  after :updated, 'whenever:update_crontab'
end
