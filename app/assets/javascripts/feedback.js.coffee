class window.FeedbackController
  constructor: ($scope, Account) ->
    $scope.user = Account.get_current()
    $scope.showAlert = false
    $scope.showThanks = false
    $scope.showForm = true

    $scope.sendFeedback = ->
      if !$scope.feedback
        $scope.showAlert = true
        return
      $scope.showForm = false
      $scope.showThanks = true
      $scope.notify("send_feedback_button_clicked")
      analytics.track('user.feedback', {
        first_name: $scope.user.first_name
        last_name: $scope.user.last_name
        user_email: $scope.user.email
        feedback: $scope.feedback
      })

window.FeedbackController.$inject = ['$scope', 'Account']