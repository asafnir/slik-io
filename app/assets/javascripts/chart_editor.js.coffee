class window.ChartController
  constructor: ($scope, $state, $stateParams, Collection, Chart) ->
    engine = new window.slikIO.Engine()
    loaded = false
    $scope.chartId = $stateParams.chart_id
    $scope.$stateParams = $stateParams
    $scope.parsedFilters = []
    $scope.chartTypes = [
      { text: 'Line', value: 'line', img: 'chart-line.svg' },
      { text: 'Column', value: 'column', img: 'chart-bar.svg' },
      { text: 'KPI', value: 'kpi', img: 'chart-kpi.svg', noDimensions: true},
      { text: 'Pie', value: 'pie', img: 'chart-pie.svg'}
    ]
    $scope.aggregationFunctions = [
      {title: 'Sum', value: 'sum'},
      {title: 'Average', value: 'avg'},
      {title: 'Max', value: 'max'},
      {title: 'Min', value: 'min'},
      {title: 'Count', value: 'cnt'}
    ]

    $scope.dimensions = csDimensions = [{}]

    $scope.chartSettings = cs = {
      dimensions: [ {} ],
      values: [ {} ]
    }

    empty = (s) -> s == null || s == ''

    onDataLoaded = (data) ->
      $scope.$apply ->
        cs.name = cs.name || data.name || ''
        cs.title = cs.title || data.title || ''
        cs.type = cs.type || data.type
        cs.collection_id = data.collection_id
        $scope.parsedFilters = for f in (data.filters || [])
          parts = /([a-zA-Z0-9_\.]*?)\ (\!=|==|>=|<=|>|<)\ ([^\ ]*?)\ *$/.exec(f)
          {operator: parts[2], field_path: parts[1], value: parts[3]}

        #For now only 1 dimension is supported
        for d in data.dimensions
          csd = csDimensions[0]
          csd.field_path = csd.field_path || d.field_path
          csd.label = csd.label || d.label || ''

        for v in data.values
          csv = cs.values[0]
          csv.field_path = csv.field_path || v.field_path
          csv.label = csv.label || v.label || ''
          csv.aggregation_function = csv.aggregation_function || v.aggregation_function

        $scope.chartSettings = cs
        loaded = true

    $scope.collections = Collection.query({with_public: true})

    getAutomaticFieldLabel = (field) ->
      return field unless field
      field.toLowerCase().replace(/(^|_)(.)/g, (m, gr0, gr) ->
        start = if gr0 == '_' then ' ' else ''
        start + gr.toUpperCase()
      )

    isAutomaticFieldLabel = (field, fieldLabel) ->
      getAutomaticFieldLabel(field) == fieldLabel

    $scope.$watch('chartSettings.dimensions[0].field_path', (v, o) ->
      return unless cs.dimensions.length > 0
      label = cs.dimensions[0].label
      if !label || isAutomaticFieldLabel(o, label)
        cs.dimensions[0].label = getAutomaticFieldLabel(v)
    )

    $scope.$watch('chartSettings.values[0].field_path', (v, o) ->
      label = cs.values[0].label
      if !label || isAutomaticFieldLabel(o, label)
        cs.values[0].label = getAutomaticFieldLabel(v)
    )

    $scope.$watch('chartSettings.type', (v) ->
      ct = currentChartType()
      cs.dimensions = if ct && true == ct.noDimensions then [] else csDimensions
    )

    $scope.addValue = () ->
      $scope.chartSettings.values.push({field_path: null, label: null, aggregation_function: 'sum'})

    $scope.getOperators = (field) ->
      return [] unless field
      return [
        {text:'=', value:'=='},
        {text:'>', value:'>'},
        {text:'<', value:'<'},
        {text:'>=', value: '>='},
        {text:'<=', value: '<='},
        {text:'!=', value: '!='}
      ]

    $scope.getFields = ->
      collection = currentCollection()
      return [] unless collection

      ({key: key, type: value} for key, value of collection.data_fields)

    currentCollection = ->
      findCollectionById(cs.collection_id)

    findCollectionById = (id) ->
      return c for c in $scope.collections when c.public_id == id

    validateConfiguration = () ->
      cs = cs
      return false unless cs.collection_id
      #return false unless cs.chart_type #Not required, since the server can auto-guess the chart_type
      return false unless cs.dimensions.length == 0 || cs.dimensions[0].field_path
      return false unless cs.values[0].field_path
      return false unless cs.values[0].aggregation_function
      true

    $scope.currentChartType = currentChartType = ->
      return ct for ct in $scope.chartTypes when ct.value == cs.type

    $scope.$watch('chartSettings.collection_id', ->
      collection = currentCollection()
      fields = $scope.getFields()
      return if fields.length == 0

      if !(csDimensions[0].field_path && $.grep(fields, (x) -> x.key == csDimensions[0].field_path)[0])
        #Selecting first date field or first field otherwise
        preferredField = $.grep(fields, (x) -> x.type == 'Datetime')[0]
        if preferredField
          csDimensions[0].field_path = preferredField.key 
        else
          csDimensions[0].field_path = fields[0].key

      if !(cs.values[0].field_path && $.grep(fields, (x) -> x.key == cs.values[0].field_path)[0])
        #Selecting first date field or first field otherwise
        preferredField = $.grep(fields, (x) -> x.type == 'Integer' || x.type == 'Float')[0]
        if preferredField
          cs.values[0].field_path = preferredField.key 
        else
          cs.values[0].field_path = fields[0].key
    )

    $scope.reload = =>
      $scope.collections = Collection.query({with_public: true},
        => buildChart()
      )

    buildChart = (configuration) ->
      engine.reload({
        chartId: $scope.chartId, 
        elementId: 'chart-preview',
        configuration: configuration, 
        onDataLoadedHandler: onDataLoaded
      })

    $scope.$watch('chartSettings', ->
      cs.values[0].aggregation_function = 'sum' if loaded && !cs.values[0].aggregation_function
      if validateConfiguration()
        buildChart(cs)
    , true)

    $scope.isCollectionValid = ->
      return (cs.collection_id || '').length > 0

    $scope.hasDataFields = ->
      return $scope.getFields().length > 0

    $scope.isFieldValid = (dimensionOrValue) ->
      (dimensionOrValue.field_path || '').length > 0

    $scope.isChartTypeValid = ->
      (cs.type || '').length > 0

    $scope.areDimensionsAndValueFieldsValid = ->
      return false if $.grep(cs.dimensions, (x) -> !$scope.isFieldValid(x)).length > 0
      $.grep(cs.values, (x) -> !$scope.isFieldValid(x)).length == 0

    $scope.validate = (shouldBeTrue) ->
      !$scope.submitAttempted || shouldBeTrue

    $scope.isReadyToSave = isReadyToSave = ->
      validation = {
        collectionValid: $scope.isCollectionValid()
        hasDataFields: $scope.hasDataFields()
        dimensionsAndValueFieldsValid: $scope.areDimensionsAndValueFieldsValid()
      }
      validation.valid = (v for k, v of validation when k.indexOf('Valid') > 0 and v == false).length == 0
      validation

    $scope.$watch('parsedFilters', ->
      # Convert filters to string representation acceptable by the chart
      $scope.chartSettings.filters = for f in $scope.parsedFilters when !empty(f.operator) and !empty(f.field_path)
        "#{f.field_path} #{f.operator} #{f.value}"
    , true)

    $scope.removeFilter = (f) ->
      index = $scope.parsedFilters.indexOf(f)
      if index > -1
        $scope.parsedFilters.splice(index, 1)

    $scope.addFilter = ->
      $scope.parsedFilters.push({operator: '==', value: null, field_path: null})

    $scope.save = ->
      $scope.notify("chart_save_button_clicked", {chart_public_id: $scope.chartId})
      $scope.submitAttempted = true
      return if $scope.loading

      validation = isReadyToSave()
      return if !validation.valid
      $scope.loading = true

      name = cs.name

      do (name) ->
        Chart.exists({name: cs.name, id: $scope.chartId}, 
          (res)->
            $scope.checkedName = name
            $scope.available = $scope.loading = res.available

            if res.available
              chart = new Chart()
              chart[key] = value for key, value of cs

              chart.is_draft = false
              chart.$update({chart_id: $scope.chartId}, (data) ->
                $scope.loading = false
                $state.transitionTo('dashboard') if data.success
              , ->
                $scope.loading = false
              )
        , ->
          $scope.loading = false
        )

    buildChart({})

window.ChartController.$inject = ['$scope', '$state', '$stateParams', 'Collection', 'Chart']
