class window.UserSessionsController 
  constructor: ($scope, $rootScope, UserSession, Account, $state) ->
    $scope.signIn = ($event) ->
      $event.preventDefault()
      UserSession.signIn(
        {user: {email: $scope.email, password: $scope.password}},
        (data) -> 
          $scope.pingLoginStatus(-> $state.transitionTo('root'))
        (data) -> 
          $scope.error = data.data.error || 'Invalid email or password'
      )
    $scope.signOut = () ->
      UserSession.signOut({},
        $rootScope.userLoggedIn = false
        $rootScope.account = null
        $state.transitionTo('user_sign_in')
      )

window.UserSessionsController.$inject = ['$scope', '$rootScope', 'UserSession', 'Account', '$state']
