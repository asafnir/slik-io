angular.module('visServices', ['ngResource'])
  .factory('Confirmation', ['$resource', ($resource) ->
    $resource('/users/confirmation', {ajax: 1}, {
      resend: {method: 'POST', isArray: false}
      activate: {method: 'GET', isArray: false}
    })
  ])
  .factory('Upload', ['$resource', ($resource) ->
    $resource('/uploads/:upload_id', {ajax: 1}, {
      status: {method: 'GET', isArray: false}
    })
  ])
  .factory('Collection', ['$resource', ($resource) ->
    $resource('/api/v1/collections/:collectionId', {ajax: 1},{
      query: {method: 'GET', isArray: true},
      get: {method: 'GET', isArray: false},
      create: {method: 'POST', isArray: false}
      'delete': {method: 'DELETE', isArray: false}
    })
  ])
  .factory('UserSession', ['$resource', ($resource) ->
    $resource('/users/:action', {ajax:1}, {
      signIn: {method: 'POST', params: {action: 'sign_in'}, isArray: false}
      signOut: {method: 'DELETE', params: {action: 'sign_out'}, isArray: false}
      current: {method: 'GET', params: {action: 'current'}, isArray: false}
    })
  ])
  .factory('Password', ['$resource', ($resource) ->
    $resource('/users/password', {ajax:1},{
      recover: {method: 'POST', isArray: false }
      change: {method: 'PUT', isArray: false }
    })
  ])
  .factory('Dashboard', ['$resource', ($resource) ->
    $resource('/dashboard', {ajax:1}, {
      query: {method: 'GET', isArray: false}
    })
  ])
  .factory('Account', ['$resource', ($resource) ->
    $resource('/accounts/:account_id', {ajax:1}, {
      get_current: {method: 'GET', params: {account_id: "me"}, isArray: false}
      test: {method: 'GET', params: {account_id: 'me', test: true}, isArray: false}
      update: {method: 'PUT', params: {account_id: "me"}, isArray: false}
      destroy: {method: 'DELETE', params: {account_id: "me"}, isArray: false}
      setCC: { method: 'PUT', url: '/accounts/:account_id/cc', params: {account_id: 'me'}, isArray: false}
    })
  ])
  .factory('Bonuses', ['$resource', ($resource) ->
    $resource('/bonuses', {ajax:1}, {
      query: {method: 'GET', isArray: false},
      useBonus: {method: 'POST', isArray: false}
    })
  ])
  .factory('Chart', ['$resource', ($resource) ->
    $resource('/api/v1/charts/:chart_id', {ajax:1}, {
      exists: {method: 'GET', url: '/api/v1/charts/exists', isArray: false}
      update: {method: 'PUT', isArray: false}
      create: {method: 'POST', isArray: false}
      'delete': {method: 'DELETE', isArray: false}
    })
  ])
  .factory('AuthorizationInterceptor', ['$q', '$injector', '$rootScope', ($q, $injector, $rootScope) ->
    (promise) ->
      promise.then(
        # Success handler
        (response) ->
          response
        # Failure handler
        (response) ->
          if response?.status == 401
            $rootScope.userLoggedIn = false
            $rootScope.account = null
            state = $injector.get('$state')
            state.transitionTo('user_sign_in')
          $q.reject(response)
      )
  ])
window.$$visAppModule = angular.module('visApp', ['ngResource', 'visServices', 'ui.router', 'blueimp.fileupload', 'ui.bootstrap'])
window.$$visAppModule.config(['$httpProvider', ($httpProvider) ->
    $httpProvider.defaults.headers.common['X-CSRF-Token']= $("meta[name='csrf-token']").attr("content")
    $httpProvider.defaults.headers.common['Content-Type']= 'application/json'
    $httpProvider.defaults.headers.common['Pragma'] = 'no-cache'
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache'
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
    $httpProvider.responseInterceptors.push('AuthorizationInterceptor');
  ])
  .directive('eatClick', -> ((scope, element, attrs)-> $(element).click((ev)->ev.preventDefault())))
  .directive('ngModelOnblur', ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, elm, attr, ngModelCtrl) ->
        return if attr.type == 'radio' || attr.type == 'checkbox'
        elm.unbind('input').unbind('keydown').unbind('change')
        elm.bind('blur', ->
          scope.$apply ->
            ngModelCtrl.$setViewValue(elm.val());
        )
    }
  )
  .directive('sendEvent', -> 
    restrict: 'A'
    scope:
      eventName: '@sendEvent'
      eventAttrs: '@eventAttrs'
    link: (scope, elm, attr) ->
      elm.click ($event) ->
        scope.$root.notify(scope.eventName, scope.$eval(scope.eventAttrs))
  )
  .run(['$state', '$rootScope', '$stateParams', '$modal', 'Account', ($state, $rootScope, $stateParams, $modal, Account) ->
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams

    $rootScope.flash = []

    $rootScope.flashSuccess = (msg) ->
      $rootScope.flash.push {msg: msg, type: 'success', added_at: new Date(), maxStates: 1, states: 0}
    $rootScope.flashError = (msg) ->
      $rootScope.flash.push {msg: msg, type: 'error', added_at: new Date(), maxStates: 1, states: 0}
    $rootScope.preserveFlash = -> $rootScope.$$preserveFlash = true

    $rootScope.pingLoginStatus = (ifLoggedInCallback) ->
      Account.get_current({},
        (account) ->
          analytics.identify(account.email, {
            email: account.email,
            first_name: account.first_name,
            last_name: account.last_name
          })
          $rootScope.userLoggedIn = true
          $rootScope.account = account
          ifLoggedInCallback() if ifLoggedInCallback
      )
      
    $rootScope.notify = (event, params) ->
      analytics.track(event, params)
    $rootScope.confirmDlg = (dlg, confirmCallback, cancelCallback = null) ->
      $modal.open({
        templateUrl: '/html/layout/_confirmation_dlg.html',
        controller: ['$scope', 'options', '$modalInstance', ($scope, options, $modalInstance) ->
          $scope.options = options
          $scope.confirm = ->
            confirmCallback() if confirmCallback
            $modalInstance.close('confirm')
          $scope.cancel = ->
            cancelCallback() if cancelCallback
            $modalInstance.dismiss('cancel')
        ]
        resolve: {options: (-> dlg) }
      })
    $rootScope.showEmbeddingInstructions = (chart_public_id) ->
      $modal.open({
        templateUrl: '/html/controls/embedding_instructions.html',
        resolve: {chart_public_id: (-> chart_public_id)}
        controller: ['$scope', '$modalInstance', 'chart_public_id', ($scope, $modalInstance, chart_public_id) ->
          $scope.chart_public_id = chart_public_id
          $scope.notify('embedding_instructions_clicked', {chart_public_id: chart_public_id})
          prettyPrint()
          $scope.close = -> $modalInstance.dismiss('close')
        ]
      })

    Account.test(
      (data) ->
        $rootScope.userLoggedIn = data.logged_in
        $rootScope.account = data.account
        $rootScope.slikIOBaseUrl = window.slikIObaseUrl
    )
  ])
