class window.DocumentationController
  constructor: ($scope, $stateParams, Collection, Account) ->
    $scope.account = Account.get_current()
    if $stateParams.collection_id
      Collection.get({collectionId: $stateParams.collection_id}, (col) -> 
        $scope.collection = col
        $scope.collection_public_id = col.public_id
      )
window.DocumentationController.$inject = ['$scope', '$stateParams', 'Collection', 'Account']