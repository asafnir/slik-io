class window.BonusesController
  constructor: ($scope, Bonuses, Account) ->
    twitter = new TwitterController(
      ->
        $scope.$apply ->
          $scope.useBonus('twitter')
    )
    text = "I just signed up to @Slik_IO - the easiest way to create and share charts"
    slik_url = "http://slik.io"
    $scope.user = Account.get_current()

    $scope.loadBonuses = ->
      Bonuses.query(loadData)

    loadData = (data) ->
      $scope.fb = new FacebookController(data.facebook_id)
      $scope.freeEvents = data.free_events
      $scope.twitterLink = "http://twitter.com/intent/tweet?text=#{text}&url=#{slik_url}"
      $scope.networks = [
        {name: "Facebook", show: data.facebook },
        {name: "Twitter", show: data.twitter },
        {name: "LinkedIn", show: data.linkedin },
        {name: "Google+", show: data.google }
      ]

    $scope.allBonusesGathered = ->
      return unless $scope.networks
      for n in $scope.networks
        if n.show
          return false
      return true

    $scope.showNetwork = (name) ->
      return unless $scope.networks
      for n in $scope.networks
        if n.name == name
          return n.show || false
      return false

    $scope.useBonus = (name) ->
      Bonuses.useBonus({network: name},
        ->
          now = new Date()
          analytics.track('app.share', {
            user_email: $scope.user.email
            network_name: name
            shared_at: now.toDateString()
          })
        , ->
      )

    $scope.facebookShare = ->
      $scope.fb.invokeShareDialog(name: "Slik.IO", link: slik_url, text: text,
        ->
          $scope.useBonus("facebook")
        , -> #do something when the user didn't complete the sharing
      )

    window.googleShare = (status) ->
      if status.type == "confirm"
        $scope.useBonus("google")

    window.linkedinShare = ->
      $scope.useBonus("linkedin")

    $scope.loadBonuses()

window.BonusesController.$inject = ['$scope', 'Bonuses', 'Account']	
