class window.SlikFileUploadController 
  constructor: ($scope, $http, $stateParams, $timeout, Chart, Upload) ->
    $scope.state = "select"
    $scope.uploadStarted = false

    $scope.getErrorMessage = ->
      message = 'An error occurred while processing the uploaded document'
      if $scope.errorMessage
        message += ': ' + $scope.errorMessage
      else
        message += '.'
      message

    $scope.showProcessingProgress = (success, fail)->
      Upload.status({upload_id: $scope.upload_id}, (data) ->
          $scope.processingMessage = "#{data.processed || 0} records processed"
          $scope.processedItems = data.processed
          if data.state == 'processing' || data.state == null
            $scope.errorMessage = null
            $timeout((() -> $scope.showProcessingProgress(success, fail)), 1000)
          else if data.state == 'done'
            $scope.errorMessage = null
            success() if success
          else if data.state == 'failure'
            $scope.errorMessage = data.error_message
            fail() if fail
      )
    Chart.get({chart_id: $stateParams.chart_id},
      (chart) -> 
        $scope.options = {
          url: "/uploads?collection_id=#{chart.collection_id}"
          dataType: 'json'
          maxNumberOfFiles: 1
          disableValidation: false
          start: (e, data) -> 
            $scope.uploadStarted = true
            $scope.state = "upload"
          done: (e, data) -> 
            $scope.upload_id = data.result.id
            $scope.showProcessingProgress(
              -> 
                $scope.state = "done"
                $scope.uploadStarted = false
                $scope.reload()
              ->
                $scope.uploadStarted = false
                $scope.state = "failure"
            )
        }
    )
window.SlikFileUploadController.$inject = ['$scope', '$http', '$stateParams', '$timeout', 'Chart', 'Upload']
