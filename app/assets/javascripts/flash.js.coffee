class window.FlashController
  constructor: ($scope, $rootScope) ->
    $rootScope.$on('$stateChangeStart',
      (event, toState, toParams, fromState, fromParams) ->
        $rootScope.flash = (s for s in $rootScope.flash when s.maxStates > s.states)
        s.states += 1 for s in $rootScope.flash
        if $rootScope.$$preserveFlash
          s.maxStates += 1 for s in $rootScope.flash
          $rootScope.$$preserveFlash = false
    )
    $scope.dismiss = (msg) -> 
      $rootScope.flash = (m for m in $rootScope.flash when m != msg)
window.FlashController.$inject = ['$scope', '$rootScope']