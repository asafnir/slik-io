# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require lib/jquery-1.10.2

#= require bootstrap
#= require unstable/angular
#= require unstable/angular-resource
#= require angular-ui-router
#= require angular-ui-bootstrap

#= require root
#= require collections
#= require chart_editor
#= require user_sessions
#= require sign_up
#= require recover_password
#= require change_password
#= require dashboard
#= require social_networks
#= require bonuses
#= require account
#= require documentation
#= require new_chart
#= require feedback
#= require file_upload
#= require confirmations
#= require flash

#= require google-code-prettify-rails/prettify

#= require lib/jquery-fileupload/vendor/jquery.ui.widget
#= require lib/jquery-fileupload/jquery.iframe-transport
#= require lib/jquery-fileupload/jquery.fileupload
#= require lib/jquery-fileupload/jquery.fileupload-process.js
# require lib/jquery-fileupload/jquery.fileupload-image
#= require lib/jquery-fileupload/jquery.fileupload-validate
#= require lib/jquery-fileupload/jquery.fileupload-angular

#= require angular_app
#= require angular_app-ui-router
#= require segmentio
