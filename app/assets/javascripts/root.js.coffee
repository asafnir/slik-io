class window.RootController
  constructor: ($scope, $state, Account) ->
    $scope.preserveFlash()
    if window.$$initialTransition.transitionTo
      $state.transitionTo($$initialTransition.transitionTo, $$initialTransition.transitionParams)
      window.$$initialTransition = {}
    else
      Account.test({},
        (d) ->
          if d.logged_in
            $state.transitionTo(if d.account.has_charts then "dashboard" else "new_chart")
          else
            $state.transitionTo("user_sign_in")
      )
window.RootController.$inject = ['$scope', '$state', 'Account']