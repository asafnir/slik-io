class window.SignUpController
  constructor: ($scope, $http, $stateParams, $state) ->
    $scope.minPasswordLength = 8
    $scope.email = $stateParams.token

    $scope.isFirstNameValid = ->
      ($scope.firstName || '').length > 0

    $scope.isLastNameValid = ->
      ($scope.lastName || '').length > 0

    $scope.isEmailFormatValid = ->
      ($scope.email || '').search(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/gi) > -1

    $scope.isEmailAvailable = ->
      $scope.email != $scope.emailChecked || $scope.emailAvailable

    $scope.isEmailValid = ->
      $scope.isEmailAvailable() && $scope.isEmailFormatValid()

    $scope.isConfirmPasswordValid = ->
      return true #Leaving this here, just in case we will need the password confirmation back in the future
      !$scope.isPasswordValid() || $scope.password == $scope.confirmPassword

    $scope.isPasswordValid = ->
      ($scope.password || '').length >= $scope.minPasswordLength

    $scope.isTosValid = ->
      true == $scope.agreeWithTos

    $scope.validate = (shouldBeTrue) ->
      !$scope.validated || shouldBeTrue

    $scope.validateAll = ->
      $scope.validated && $scope.isEmailFormatValid() && $scope.isPasswordValid() && $scope.isConfirmPasswordValid() && $scope.isTosValid()

    $scope.addGAConversionIframe = () ->
      container = document.getElementById("signupSuccessContainer")
      iframe = document.createElement("iframe")
      iframe.setAttribute("height", "0px")
      iframe.setAttribute("width", "0px")
      iframe.setAttribute("src", "/html/adwords/_signed_up_conversion.html")
      container.appendChild(iframe)

    $scope.signUp = ($event) ->
      $event.preventDefault()
      return if $scope.loading
      $scope.validated = true
      return if not $scope.validateAll()
      email = $scope.email
      $scope.loading = true
      do (email) ->
        $http.get("/users/email_available", {
            params: {
              email: email
            }
          })
          .success( (data) ->
            $scope.emailChecked = email
            $scope.emailAvailable = data.available
            $scope.loading = false #In case we return on next statement
            return unless $scope.emailAvailable
            
            $scope.loading = true
            $http.post('/users', {
                user: {
                  first_name: $scope.firstName
                  last_name: $scope.lastName
                  email: email
                  password: $scope.password
                  password_confirmation: $scope.confirmPassword
                }
              }).success((data, status, headers, config)->
                $scope.addGAConversionIframe()
                $scope.accountCreated = true
              ).error ->
                $scope.loading = false
          )
          .error ->
            $scope.loading = false

window.SignUpController.$inject = ['$scope', '$http', '$stateParams']