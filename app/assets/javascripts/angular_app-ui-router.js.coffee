window.$$visAppModule.config(['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise("/")
  # $urlRouterProvider.when("/", ['$state', '$rootScope', ($state, $rootScope) ->
  #   return unless $rootScope.userLoggedIn == true || $rootScope.userLoggedIn == false
  #   dest = if $rootScope.userLoggedIn then 'dashboard' else 'user_sign_in'
  #   $state.transitionTo(dest)
  # ])
  $stateProvider
    .state('root', {
      url: '/',
      controller: window.RootController
    })
    .state('account', {
      url: '/my_account',
      templateUrl: '/html/accounts/index.html',
      controller: window.AccountController
    })
    .state('account_billing', {
      url: '/my_account/billing'
      templateUrl: '/html/accounts/index.html'
      controller: window.AccountController
    })
    .state('account_traffic', {
      url: '/my_account/traffic'
      templateUrl: '/html/accounts/index.html'
      controller: window.AccountController
    })
    .state('activate_account', {
      url: '/activate_account/:confirmation_token',
      templateUrl: '/html/users/resend_activation.html',
      controller: window.ConfirmationsController
    })
    .state('chart_edit', {
      url: '/charts/edit/:chart_id',
      templateUrl: '/html/charts/show.html',
      controller: window.ChartController
    })
    .state('new_chart', {
      url: '/charts/new',
      templateUrl: '/html/charts/new.html',
      controller: window.NewChartController
    })
    .state('new_chart.documentation', {
      url: '/documentation/:collection_id',
      templateUrl: '/html/documentation/index.html',
      controller: window.DocumentationController
    })
  # Users Sign up/in
    .state('user_sign_in', {
      url: '/users/sign_in'
      templateUrl: '/html/users/sign_in.html'
      controller: window.UserSessionsController
    })
    .state('user_resend_activation', {
      url: '/users/resend_activation',
      templateUrl: '/html/users/resend_activation.html',
      controller: window.ConfirmationsController
    })
    .state('user_sign_up',{
      url: '/users/sign_up?token'
      templateUrl: '/html/users/sign_up.html'
      controller: window.SignUpController
    })
    .state('password_recover',{
      url: '/users/recover_password'
      templateUrl: '/html/users/recover_password.html'
      controller: window.RecoverPasswordController
    })
    .state('password_change', {
      url: '/users/change_password?reset_password_token'
      templateUrl: '/html/users/change_password.html'
      controller: window.ChangePasswordController
    })
  #Dashboard
    .state('dashboard', {
      url: '/dashboard'
      templateUrl: '/html/dashboard/index.html'
      controller: window.DashboardController
    })
  #Bonuses
    .state('bonuses', {
      url: '/bonuses'
      templateUrl: '/html/bonuses/index.html'
      controller: window.BonusesController
    })
    .state('privacy', {
      url: '/privacy'
      templateUrl: '/html/privacy.html'
    })
    .state('tos', {
      url: '/tos'
      templateUrl: '/html/tos.html'
    })
    .state('pricing',{
      url: '/pricing'
      templateUrl: '/html/pricing.html'
    })
    .state('documentation', {
      url: '/documentation?collection_id'
      templateUrl: '/html/documentation/index.html',
      controller: window.DocumentationController
    })
    .state('feedback', {
      url: '/feedback'
      templateUrl: '/html/feedback/index.html'
      controller: window.FeedbackController
    })
])