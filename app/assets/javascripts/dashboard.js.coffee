class window.DashboardController
  constructor: ($scope, $state, Dashboard, Collection, Chart) ->
    Dashboard.query((data) -> 
      $scope.data = data
    )
    reloadCollections = -> Collection.query({include_public_with_my_charts: true}, (data) -> 
      $scope.collections = data
      setTimeout((-> slikIO.engine.reload()), 500)
    )
    $scope.deleteCollection = (col) ->
      $scope.confirmDlg(
        {title: "Delete a collection", body: "Collection will be deleted with all its charts. Are you sure you want to delete this collection?", confirmButtonText: "Delete"}
        -> Collection.delete({collectionId: col.id},
          -> reloadCollections()
        )
      )
    $scope.deleteChart = (chart) ->
      $scope.confirmDlg(
        {title: "Delete a chart", body: "Are you sure you want to delete this chart?", confirmButtonText: "Delete"}
        -> Chart.delete({chart_id: chart.public_id},
          -> reloadCollections()
        )
      )

    $scope.createChart = (collection) =>
      Chart.create({is_draft: true, collection_id: collection.public_id}, 
        (c) -> 
          $state.transitionTo('chart_edit', {chart_id: c.id})
          $scope.notify("created_chart_from_existing_col", {public_id: c.id, public_id: c.public_id})
      )

    reloadCollections()
  	
window.DashboardController.$inject = ['$scope', '$state', 'Dashboard', 'Collection', 'Chart']
