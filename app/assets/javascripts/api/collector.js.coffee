#= require api/jsonp

class SlikIOCollector
  constructor: () ->
    @baseUrl = window.slikIObaseUrl || "https://app.slik.io"
    JSONP.init(callbackName: 'callback')
  sendData: (collection_id, data) ->
    JSONP.get("#{@baseUrl}/api/v1/collections/#{collection_id}/data", {data: JSON.stringify(data), api_public_key: window.SLIKIO_PUBLIC_KEY}, (->))

window.slikIOCollector = new SlikIOCollector()