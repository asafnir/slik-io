class Renderer
  constructor: (@chart_raw_data) ->
    @datum = @transformData(@chart_raw_data)
  getRawChartData: => @chart_raw_data
  getDatum: => @datum
  transformData: (chart_data) =>
    result = []
    for s in chart_data.series
      for d in s.data
        for x, index in d
          if index > 0
            zbIndex = index - 1       # Zero-Based index
            result[zbIndex] = {values: []} unless result[zbIndex]
            result[zbIndex].key = s.labels[index]
            result[zbIndex].type = if chart_data.type == 'column' then 'bar' else 'line'
            result[zbIndex].disabled = false
            result[zbIndex].yAxis = 1
            result[zbIndex].values.push({x: d[0], y: d[index]})
    return result
  render: =>
class PieChartRenderer extends Renderer
  getDatum: =>
    @datum[0].values
  render: ->
    chart = nv.models.pieChart()
      .x((d) -> d.x)
      .y((d) -> d.y)
      .showLabels(true)
    chart

class LineChartRenderer extends Renderer
  render: ->
    chart_data = @getRawChartData()

    chart = (if chart_data.type == 'line' then nv.models.lineChart() else nv.models.multiBarChart().showControls(false))
      .margin(top: 30, right: 60, bottom: 50, left: 70)

    chart.xAxis
      .axisLabel(chart_data.series[0].labels[0])
      .tickFormat(chart_data.series[0].type_formatters[0])
    chart.yAxis
      .tickFormat(chart_data.series[0].type_formatters[1])

    nv.utils.windowResize(-> chart.update())

    chart

class KpiChartRenderer extends Renderer
  render: -> window.slikIO.charts.KpiChart()

class ColumnChartRenderer extends Renderer
  render: ->
    chart_data = @getRawChartData()
    chart = nv.models.multiBarChart()
      .showControls(false)
      .margin(top: 30, right: 60, bottom: 50, left: 70)
    chart.xAxis
      .tickFormat(chart_data.series[0].type_formatters[0])
    chart.yAxis
      .tickFormat(chart_data.series[0].type_formatters[1])
    nv.utils.windowResize(-> chart.update())

    chart

window.slikIO.renderers.PieChartRenderer = PieChartRenderer
window.slikIO.renderers.LineChartRenderer = LineChartRenderer
window.slikIO.renderers.ColumnChartRenderer = ColumnChartRenderer
window.slikIO.renderers.KpiChartRenderer = KpiChartRenderer

