class UrlBuilder
  getBaseUrl: () -> window.slikIObaseUrl || 'https://app.slik.io'

  #Adds the obj parameters to the URL
  addObjectParameters: (url, obj) ->
    addObjectParametersInternal(null, "#{@getBaseUrl()}#{url}", obj)

  type = (obj) ->
    if obj == undefined or obj == null
      return String obj
    classToType = new Object
    for name in "Boolean Number String Function Array Date RegExp".split(" ")
      classToType["[object " + name + "]"] = name.toLowerCase()
    myClass = Object.prototype.toString.call obj
    if myClass of classToType
      return classToType[myClass]
    return "object"

  #Adds the obj parameters to the URL (internal routine)
  addObjectParametersInternal = (prefix, url, obj, mutator) ->
    #Mutator is used to modify keys and values of object properties. Returning null will skip the serialization of a specific property
    for key, value of obj

      if mutator
        mutated = mutator(key, value)
        continue unless mutated
        [key, value] = mutated

      continue if value == undefined
      continue if key.indexOf('$$') == 0
      continue if true == obj['$$skip_' + key]

      valueType = type(value)
      continue if valueType == 'function'

      p = "#{prefix}[#{key}]" if prefix
      p = "#{key}" if not prefix

      if valueType == 'array'
        url = addArrayParameters(p, url, value)
      else if type(value) == 'object'
        url = addObjectParametersInternal("#{p}", url, value)
      else
        url = addParameter(url, "#{p}", value)
    url

  addArrayParameters = (prefix, url, array) ->
    index = 0
    for value in array
      array_prefix = "#{prefix}[]"
      vtype = type(value)

      if vtype == 'array'
        url = addArrayParameters(array_prefix, url, value)
      else if vtype == 'object'
        url = addObjectParametersInternal(array_prefix, url, value)
      else
        url = addParameter(url, array_prefix, value)
      index++
    url

  #Adds single parameter to URL. Adds ? and & as necessary
  addParameter = (url, parameterName, parameterValue) ->
    url = url + '?' unless url.match(/\?/g)
    url = url + '&' unless url.match(/\?$/g)
    url = url + parameterName + '=' + encodeURIComponent(parameterValue)
    url

  #Adds multiple parameters to url
  addParameters = (url, parameters) ->
    for p in parameters
      url = addParameter(url, p.key, p.value)
    url

window.slikIO.UrlBuilder = UrlBuilder
