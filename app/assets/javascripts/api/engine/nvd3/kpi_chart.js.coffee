KpiChart = -> 
  class Chart
    constructor: (selection) ->
      width = @width
      height = @height
      noData = '-'
      selection.each (data)->
        container = d3.select(@)
        availableWidth = width || parseInt(container.style('width')) || 960
        availableHeight = height || parseInt(container.style('height')) || 400

        titleContainerHeight = availableHeight * 0.2
        valueSize = availableHeight * 0.7
        titleSize = availableHeight * 0.12

        textAdjustValue = (size) ->
          Math.floor(size*0.39)

        adjustFontSizeToFit = (text, initialFontSize, sizeToFit) ->
          #Voodoo magic
          x = text[0]
          return unless x
          x = x[0]
          return unless x

          fontSize = initialFontSize
          first = true
          iteration = 0
          while first || iteration < 20 && x.textLength.baseVal.value > sizeToFit
            text.style('font-size', fontSize)
              .attr('dy', textAdjustValue(fontSize))
            fontSize *= 0.95
            
            first = false
            iteration++

        #if !data || !data.series || !data.series.length || !data.series[0] || !data.series[0].data || !data.series[0].data.length || !data.series[0].data[0] || !data.series[0].data[0][1]
        if !data || !data.length || data.length == 0 || !data[0].values || !data[0].values.length || data[0].values.length == 0
          dataItem = noData
        else
          #dataItem = data.series[0].data[0][1]
          dataItem = data[0].values[0][1]

        container.attr('viewBox', "0 0 #{availableWidth} #{availableHeight}").attr('preserveAspectRatio', 'meet')

        g = container.selectAll('g').data([data]).enter()
          .append('g')

        titleContainer = g.selectAll('.titleContainer').data([data.title]).enter()
          .append('rect')
          .attr('class', 'titleContainer')
          .attr('x', 0).attr('y', availableHeight - titleContainerHeight)
          .attr('width', availableWidth).attr('height', titleContainerHeight)
          .attr('fill', '#88eb88')

        background = g.selectAll('.background').data([data]).enter()
          .append('rect')
          .attr('class', 'background')
          .attr('x', 0).attr('y', 0).attr('width', availableWidth).attr('height', availableHeight - titleContainerHeight)
          .attr('fill', '#77cc77')

        valueText = g.selectAll('.value').data([dataItem]).enter()
          .append('text').attr('class', 'value')
          .style('text-anchor', 'middle')
          .style('font-size', valueSize)
          .attr('x', availableWidth / 2).attr('y', (availableHeight - titleContainerHeight)/2)
          .attr('fill', 'white')
          .attr('dy', textAdjustValue(valueSize))
          
        valueText.text((d) -> d)

        adjustFontSizeToFit(valueText, valueSize, availableWidth)

        valueText.attr('class', 'nvd3 nv-noData') if dataItem == noData

        titleText = g.selectAll('text.title').data([data.title]).enter()
          .append('text').attr('class', 'title')
          .style('text-anchor', 'middle')
          .style('font-size', titleSize)
          .style('font-family', 'Tahoma')
          .style('font-weight', 'normal')
          .attr('fill', 'white')
          .attr('dy', textAdjustValue(titleSize))
          .attr('x', availableWidth/ 2).attr('y', availableHeight - titleContainerHeight/2)
          .text((d) -> (d || '').toUpperCase())

    update: ->
      container.transition().duration(@transitionDuration).call(Chart)

  return Chart;

window.slikIO.charts.KpiChart = KpiChart
