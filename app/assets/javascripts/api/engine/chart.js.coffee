class Chart
  constructor: (options = {}) ->
    @containerEl = options.containerElement
    @configuration = options.configuration
    @onDataLoadedHandlers = if options.onDataLoadedHandler then [options.onDataLoadedHandler] else []

    @chartId = options.chartId || @containerEl.getAttribute("data-chart-id")
    @containerEl.setAttribute("data-chart-id", @chartId)
    @credits = {url: "http://slik.io", text: "Created using Slik.IO", target: "_blank"}

    @containerSel = "[data-chart-id='#{@chartId}']"
    @selectors = {
      root: @containerSel,
      header: "#{@containerSel} .chart-header",
      chart: "#{@containerSel} .chart-container",
      controls: "#{@containerSel} .chart-controls"
    }
    @isControlBarEnabled = options.enableControlsBar || true
    @urlBuilder = new window.slikIO.UrlBuilder()
    @renderersMap = {
      'column': window.slikIO.renderers.ColumnChartRenderer,
      'line': window.slikIO.renderers.LineChartRenderer,
      'kpi': window.slikIO.renderers.KpiChartRenderer,
      'pie': window.slikIO.renderers.PieChartRenderer
    }

    @init()
    @loadChartData(@render)

  init: () ->
    @containerEl.setAttribute("height", "250px") unless @containerEl.hasAttribute("height")
    #Removing all previous elements
    d3.select(@selectors[x]).remove() for x in ['header', 'chart', 'controls']

    # Create header div
    @headerEl = d3.select(@selectors.root).append("div").attr("class", "chart-header")
    @creditsEl = @headerEl.append("a").attr("href", @credits.url)
      .attr("class", "credits")
      .attr("target", @credits.target)
      .text(@credits.text)
    @titleEl = @headerEl.append("h1")

    # Create chart container
    @chartEl = d3.select(@selectors.root)
      .append("div").attr("class", "chart-container")
      .append("svg")

    # Create control bar container 
    if @isControlBarEnabled
      @controlBarEl = d3.select(@selectors.root).append("div").attr("class", "chart-controls")
      new window.slikIO.ChartControls(@controlBarEl, @chartId)

  loadChartData: (callback) ->
    url = @urlBuilder.addObjectParameters("/api/v1/charts/#{@chartId}", @configuration)
    JSONP.get(url, {}, (response) => 
      processed_data = @processData(response)
      cb(processed_data) for cb in @onDataLoadedHandlers
      callback(processed_data)
    )

  renderNoDataMessage: ->
    messageContainer = d3.select(@containerSel).append("div").attr("class", "no-data-message-container")
    messageContainer.append('div').attr('class', 'no-data-message')
      .text('No data to display')

  render: (data) =>
    @titleEl.text(data.title)

    nonEmptySeries = (x for x in data.series when x.data.length > 0)

    if nonEmptySeries.length == 0
      @renderNoDataMessage()
      return

    renderer = new @renderersMap[data.type || "line"](data)
    @chartEl
      .datum(renderer.getDatum())
      .transition().duration(300)
      .call(renderer.render())

  processData: (chart_data) ->
    for s in chart_data.series
      s.type_formatters = []
      for t, i in s.types
        s.type_formatters[i] = if t == 'Datetime'
          d[i] = d3.time.format("%Y-%m-%dT%H:%M:%SZ").parse(d[i]) for d in s.data 
          (x) -> d3.time.format('%x')(new Date(x))
        else if t == 'String'
          (x) -> x
        else
          (x) -> d3.format(',f')(x)
    chart_data


window.slikIO.Chart = Chart
