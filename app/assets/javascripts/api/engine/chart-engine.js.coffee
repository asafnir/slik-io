class Engine
  constructor: (options)->
    @onChartDataLoaded = []
    @urlBuilder = new window.slikIO.UrlBuilder()
    @baseUrl = window.slikIObaseUrl || "https://app.slik.io"
    JSONP.init(callbackName: 'callback')
    @addCss()
    @reload()

  addCss: -> 
    d3.select("head").append("link")
      .attr("type", "text/css")
      .attr("rel", "stylesheet")
      .attr("href", "#{@urlBuilder.getBaseUrl()}/assets/embedded_chart.css")

  buildChart: (chart_id, chart_data) =>
    #create the share bar
    if chart_data.series.length != 0 #not empty chart
      shareBar = new ChartShareBar(@baseUrl, controlsSelector, chart_id)

  reload: (options) ->
    options = options || {}
    elements = []
    if options.element
      elements = [options.element]
    else if options.elementId
      elements = [document.getElementById(options.elementId)]
    else if options.chartId 
      elements = el for el in document.getElementsByTagName("div") when el.getAttribute("data-chart-id") == options.chartId
    else
      elements = (el for el in document.getElementsByTagName("div") when el.hasAttribute("data-chart-id"))
    new window.slikIO.Chart({
      chartId: options.chartId, 
      containerElement: el, 
      configuration: options.configuration,
      onDataLoadedHandler: options.onDataLoadedHandler
    }) for el in elements
      
  debug: (msg)->
    window.console.log msg if window.console

window.slikIO.Engine = Engine
window.slikIO.engine = new Engine()
