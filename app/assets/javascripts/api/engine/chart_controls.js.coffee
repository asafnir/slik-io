class ChartControls
  constructor: (container, chart_id) ->
    urlBuilder = new window.slikIO.UrlBuilder()
    baseUrl = urlBuilder.getBaseUrl()

    SN = window.SocialNetworks
    shareButtons = [
      {windowUrl: SN.facebookChartShareLink(chart_id), icon: 'icon-facebook.png', class: 'chart-share-facebook'},
      {windowUrl: SN.twitterChartShareLink(chart_id), icon: 'icon-twitter.png', class: 'chart-share-twitter'},
      {windowUrl: SN.linkedinChartShareLink(chart_id), icon: 'icon-linkedin.png', class: 'chart-share-linkedin'},
      {windowUrl: SN.googleChartShareLink(chart_id), icon: 'icon-google-plus.png', class: 'chart-share-google'},
      {
        linkUrl: "mailto:?subject=Check out this awesome chart by Slik.IO&body=http://app.slik.io/charts/#{chart_id}",
        icon: 'icon-email.png',
        class: 'chart-share-email'
      }
    ]

    shareLinks = container.selectAll("a")
      .data(shareButtons)
      .enter()
        .append("a")
          .attr("class", "link-icon pull-right")
          .attr("href", (d) -> "#")
    addButtonImage = (selection) -> selection.append("img")
      .attr("class", (d) -> "#{d.class} chart-share-button")
      .attr("src", (d) -> "#{baseUrl}/assets/#{d.icon}")

    shareLinks.filter((x) -> !!x.windowUrl)
      .attr("onclick", (d) -> "window.open('#{d.windowUrl}', 'popUpwindow', 'height=500,width=800'); return false;")
      .call(addButtonImage)
    shareLinks.filter((x) -> !!x.linkUrl)
      .attr("href", (d) -> d.linkUrl)
      .call(addButtonImage)

window.slikIO.ChartControls = ChartControls
