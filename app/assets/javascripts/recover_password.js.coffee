class window.RecoverPasswordController
  constructor: ($scope, Password) ->

    $scope.isEmailValid = ->
      ($scope.email || '').search(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/gi) > -1

    $scope.validate = (shouldBeTrue) ->
      !$scope.submitAttempted || shouldBeTrue

    $scope.recover = ($event) ->
      $event.preventDefault()
      $scope.submitAttempted = true

      return if $scope.loading

      $scope.result = null

      return unless $scope.isEmailValid()

      $scope.loading = true

      Password.recover({user: { email: $scope.email }},
        ->
          $scope.loading = false
          $scope.result = 'success'
        ,
        ->
          $scope.result = 'fail'
          $scope.loading = false
      )

window.RecoverPasswordController.$inject = ['$scope', 'Password']