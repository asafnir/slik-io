class window.CollectionsController
  constructor: ($scope, Collection) ->
    $scope.collections = Collection.query()

window.CollectionsController.$inject = ['$scope', 'Collection']

class window.CollectionController
  constructor: ($scope, $stateParams, Collection) ->
    $scope.collection = Collection.get({collectionId: $stateParams.collectionId})
window.CollectionController.$inject = ['$scope', '$stateParams', 'Collection']