class window.AccountController 
  constructor: ($scope, Account, $state) ->
    $scope.account = Account.get_current()
    $scope.saveAccount = () ->
      $scope.accountSavedStatus = 'In progress'
      a = $scope.account
      Account.update({
        first_name: a.first_name,
        last_name: a.last_name
      }, ->
        $scope.accountSavedStatus = 'Success'
      , ->
        $scope.accountSavedStatus = 'Fail'
      )

    $scope.cc = {}

    $scope.$watch('cc', (cc)->
      cc.numberValid = Stripe.card.validateCardNumber(cc.number)
      cc.expirationDateValid = Stripe.card.validateExpiry(cc.month, cc.year)
      cc.cvvValid = Stripe.card.validateCVC(cc.cvv)
    , true)

    $scope.years = (new Date().getFullYear() + i for i in [0..10])

    $scope.validate = (shouldBeTrue) ->
      !$scope.submitAttempted || shouldBeTrue

    getMask = (fallback) ->
      ccNumber = $scope.cc.number || ''
      last4Index = ccNumber.search(/\d([-\.\s]*?\d){3}$/g)
      return fallback if last4Index < 0
      mask = ccNumber.substring(0, last4Index).replace(/\d/g, 'x')
      mask += ccNumber.substring(last4Index)
      mask

    stripeResponseHandler = (status, response) =>
      cc = $scope.cc
      $scope.$apply ->
        if response.error
          $scope.loading = false
          cc.error = $scope.error.message
        else
          mask = getMask(response.card.last4)
          Account.setCC({
            token: response.id
            mask: mask
          }, (d) ->
            $scope.loading = false

            #Updating the global state
            if d.success
              $scope.account.has_cc_attached = true
              $scope.account.cc_mask = mask
              $scope.replaceCC = false
              $scope.cc = {}
          , ->
            $scope.loading = false
            cc.error = 'An error occurred while saving the billing infromation. Please try again.'
          )

    $scope.showCCForm = ->
      !$scope.account.has_cc_attached || $scope.replaceCC

    $scope.saveCC = ->
      cc = $scope.cc
      $scope.submitAttempted = true

      return if $scope.loading
      return unless cc.numberValid && cc.cvvValid && cc.expirationDateValid

      cc.error = null

      $scope.loading = true
      $scope.notify("billing_info_saved")
      Stripe.card.createToken({
        number: cc.number,
        cvc: cc.cvv,
        exp_month: cc.month,
        exp_year: cc.year,
      }, stripeResponseHandler)

    activateTabForState = (name)->
      index = name.indexOf('_')
      if index > -1
        pane_id = name.substr(index + 1) + 'Information'
        if pane_id
          tab = $("a[href='##{pane_id}']")
          tab.tab('show')

    activateTabForState($state.current.name)

window.AccountController.$inject = ['$scope', 'Account', '$state']