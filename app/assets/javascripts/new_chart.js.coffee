class window.NewChartController
  constructor: ($scope, $state, Collection, Chart) ->
    $scope.collections = []
    Collection.query({with_public: true}, (data) -> 
      $scope.collections = data
      $scope.private_collections = (c for c in data when c.user_id == $scope.account.id)
      $scope.public_collections = (c for c in data when c.user_id != $scope.account.id)
    )
    $scope.selectDatasource = (ds) ->
      $scope.selected_collection = null
      $scope.collection = null
      $scope.datasource = ds
    $scope.createCollection = () ->
      Collection.create({name: $scope.new_collection_name}, (col) ->
        $scope.hide_collection_types = true
        $scope.collection = col
        $scope.notify('collection_created', {name: col.name, id: col.id, public_id: col.public_id})
        $state.transitionTo('new_chart.documentation', {collection_id: col.id})
      )
    $scope.createChart = () ->
      map = {
        'public': {col_id: (-> $scope.selected_collection.public_id), event: "created_chart_from_public_col"},
        'existing': {col_id: (-> $scope.selected_collection.public_id), event: "created_chart_from_existing_col"},
        'create': {col_id: (-> $scope.collection.public_id), event: 'created_chart_from_new_col'}
      }
      eventName = map[$scope.datasource].event
      collection_id = map[$scope.datasource].col_id()
      Chart.create({is_draft: true, collection_id: collection_id}, 
        (c) -> 
          $state.transitionTo('chart_edit', {chart_id: c.id})
          $scope.notify(eventName, {public_id: c.id, public_id: c.public_id})
      )

window.NewChartController.$inject = ['$scope', '$state', 'Collection', 'Chart']