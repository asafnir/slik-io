class window.FacebookController
	constructor: (app_id) ->
		$.getScript("http://connect.facebook.net/en_US/all.js")

		window.fbAsyncInit = ->
			FB.init({ appId: app_id ,status: true,xfbml: true })

	invokeShareDialog : (options, onSuccess, onError) ->
    FB.ui({method: 'feed',name: options.name, link: options.link, caption: options.name, description: options.text},
      (response) -> 
      	if (response && response.post_id)
      		onSuccess(response.post_id)
      	else
      		onError()
    )
     

class window.TwitterController
	constructor: (onTweet) ->
		$.getScript("http://platform.twitter.com/widgets.js", () ->
		  twttr.ready((twttr) ->
		 	  twttr.events.bind('tweet', (event) ->
		 	  	onTweet()
		 	  )
		  )
		)

class window.SocialNetworks
	baseUrl = "http://app.slik.io"

	@facebookChartShareLink: (chart_id) ->
    "https://www.facebook.com/sharer/sharer.php?s=100"+
    "&p[url]=#{baseUrl}/charts/#{chart_id}"+
    "&p[title]=Slik.IO - Charts as a service"+
    "&p[summary]=Awesome! I created this chart using @Slik.IO - the easiest way to visualize data"
	
	@twitterChartShareLink: (chart_id) ->
    "http://twitter.com/intent/tweet?"+
    "&text=Awesome! I created this chart using @Slik_IO - the easiest way to visualize data"+
    "&url=#{baseUrl}/charts/#{chart_id}"

  @linkedinChartShareLink: (chart_id) ->
  	"http://www.linkedin.com/shareArticle?mini=true" + 
    "&url=#{baseUrl}/charts/#{chart_id}" +
    "&title=Slik.IO - Charts as a service" +
    "&summary=Awesome! I created this chart using @Slik.IO - the easiest way to visualize data"

  @googleChartShareLink: (chart_id) ->
  	"https://plus.google.com/share?url=#{baseUrl}/charts/#{chart_id}"