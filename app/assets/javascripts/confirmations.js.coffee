class window.ConfirmationsController
  constructor: ($scope, $state, $stateParams, Confirmation) ->
    #console.log $stateParams
    if $stateParams.confirmation_token
      Confirmation.activate({confirmation_token: $stateParams.confirmation_token},
        ->
          $scope.flashSuccess("Account activated successfully. You're now logged in.") 
          $scope.pingLoginStatus(-> $state.transitionTo('root'))
        -> 
          $scope.flashError("Invalid confirmation url")
      )
    $scope.resend = (ev) ->
      ev.preventDefault()
      
      if ($scope.email || '').search(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/gi) == -1
        $scope.error = "Email address is invalid"
        return
      Confirmation.resend({user: {email: $scope.email}},
        ->
          $scope.activationEmailSent = true
        ->
      )

window.ConfirmationsController.$inject = ['$scope', '$state', '$stateParams', 'Confirmation']