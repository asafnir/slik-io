class window.ChangePasswordController
  constructor: ($scope, $stateParams, Password) ->
    $scope.minPasswordLength = 8

    $scope.isPasswordValid = ->
      ($scope.password || '').length >= $scope.minPasswordLength

    $scope.validate = (shouldBeTrue) ->
      !$scope.submitAttempted || shouldBeTrue

    $scope.change = ($event) ->
      $event.preventDefault()
      $scope.submitAttempted = true

      return if $scope.loading

      $scope.result = null

      return unless $scope.isPasswordValid()

      $scope.loading = true

      user = {
        reset_password_token: $stateParams.reset_password_token,
        password: $scope.password
      }

      Password.change(user,
        (data) ->
          $scope.loading = false
          $scope.result = if data.success then 'success' else 'fail'
        , ->
          $scope.loading = false
          $scope.result = 'fail'
      )

window.ChangePasswordController.$inject = ['$scope', '$stateParams', 'Password']