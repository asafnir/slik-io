class ChartValue
  include Mongoid::Document

  AGGREGATIONS = {
    sum: 'Sum',
    avg: 'Average',
    max: 'Maximum',
    min: 'Minimum',
    cnt: 'Count'
  }

  field         :field_path,              type: String
  field         :label,                   type: String
  field         :aggregation_function,    type: String

  embedded_in     :chart

  def mongo_field_path; field_path.gsub('|', '.'); end
end
