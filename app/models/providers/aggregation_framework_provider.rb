module Providers
  class AggregationFrameworkProvider
    def initialize collection 
      @collection = collection; 
    end

    def mongo_collection
      @mongo_collection ||= @collection.mongo_collection
    end

    def aggregate options
      values = options[:values]
      dimension_hash = options[:dimensions] ? options[:dimensions][0] : nil
      filters = options[:filters]

      id = translate_dimension_to_aggregation_id(dimension_hash)
      unwinds = translate_arrays_to_unwinds(dimension_hash, values)
      matches = translate_filters_to_matches(filters)
      group = {"_id" => id}
      values.each_index{|i| 
        v = values[i]
        group["aggr_#{i}"] = convert_value_to_aggregate_expression(v)
      }

      mongo_collection.aggregate(
        *unwinds,
        *matches,
        { "$group" => group },
        { "$sort" => { "_id" => 1 } }
      ).map{ |x|
        record = [x["_id"]]
        values.each_index{|i| 
          record += [x["aggr_#{i}"]]
        }
        record
      }
    end

    private

    # Translates filters to mongo $match clauses
    #
    # Each filter should be in the format (SPACES between expression parts are MANDATORY):
    #   <field_path> <operator> <value>
    #
    # <operator> is one of the following:
    #   < > <= >= ==
    #
    # Example 1
    #   Filter: "user.id == 123"
    #   Translates to:
    #     {$match: { "user.id" => 123 } }
    #
    # Example 2
    #   Filter: "created_at >= 2008-03-04"
    #   Translates to:
    #     {$match: { "created_at" => { "$gt" => Date.iso8601(2008-03-04) } } }
    def translate_filters_to_matches filters
      return [] unless filters

      matches = []
      filters.each {|f|
        next unless f

        parts = /([a-zA-Z0-9_\.]*?)\ (\!=|==|>=|<=|>|<)\ ([^\ ]*?)\ *$/.match(f)
        field = parts[1]
        operator = parts[2]

        value = parts[3]
        string_match = value.match(/^'(.*?)'$/)       # Remove the quotes if present
        value = string_match[1] if string_match

        # Depending on field type we'll pass the corresponding value to mongo
        value = case @collection.field_type field
        when 'Datetime' then Parsing::ValueParser.parse_date(value)
        when 'Integer' then value.to_i
        when 'Float' then vlaue.to_f
        else value
        end

        match_obj = case operator
        when "==" then value
        when "!=" then {"$ne" => value}
        when ">=" then {"$gte" => value}
        when "<=" then {"$lte" => value}
        when "<" then {"$lt" => value}
        when ">" then {"$gt" => value}
        else raise Exception.new("Unknown operator: #{operator}")
        end

        matches << {"$match" => {field => match_obj}}
      }
      matches
    end

    # Unwinds any arrays
    # Example: 
    #   Given data:
    #     {customer: {email: "john@slik.io"}, transactions: [{amount: 100}, {amount: 50}]}
    #     {customer: {email: "dan@slik.io"}, transactions: [{amount: 10}, {amount: 40}]}
    #     {customer: {email: "steve@slik.io"}, transactions: []}
    #
    #   and given we want to calculate sum(transactions.amount), the data SHOULD be unwinded to:
    #
    #     {customer: {email: "john@slik.io"}, transactions: {amount: 100}}
    #     {customer: {email: "john@slik.io"}, transactions: {amount: 50}}
    #     {customer: {email: "dan@slik.io"}, transactions: {amount: 10}}
    #     {customer: {email: "dan@slik.io"}, transactions: {amount: 40}}
    #
    #   NOTE that the resulting dataset doesn't contain records for "steve@slik.io" as he has no transactions
    def translate_arrays_to_unwinds dimension, values
      unwinds = []
      [dimension ? dimension[:field_path] : nil] + values.map{|v| v[:field_path]}.each { |field_path|
        next unless field_path.nil? || field_path.index(UserCollection::FIELD_SEPARATOR)

        pos = nil
        while pos = field_path.index(UserCollection::FIELD_SEPARATOR, (pos || 0) + 1)
          field_subpath = field_path[0..pos - 1]
          next unless @collection.data_fields[field_subpath] == 'Array'
          unwinds << {"$unwind" => "$#{field_subpath}"} unless unwinds.include?({"$unwind" => "$#{field_subpath}"})
          field_path = field_path[pos + 1..-1]
        end
      }
      unwinds
    end

    # Returns the object to group by
    def translate_dimension_to_aggregation_id dimension_hash
      # If dimension_hash is nil (for KPIs), we'll group by an empty string
      # which means - we'll run the aggregation function across all the records in the collection
      return nil unless dimension_hash

      # Otherwise - we'll group by the dimension field
      "$#{dimension_hash[:field_path]}"
    end

    # Maps our aggregation functions to mongodb aggregation framework functions
    def convert_value_to_aggregate_expression value
      case value[:aggregation_function].to_s
      when 'sum' then {"$sum" => "$#{value[:field_path]}"}
      when 'avg' then {"$avg" => "$#{value[:field_path]}"}
      when 'max' then {"$max" => "$#{value[:field_path]}"}
      when 'min' then {"$min" => "$#{value[:field_path]}"} 
      when 'cnt' then {"$sum" => 1 }
      end
    end
  end
end