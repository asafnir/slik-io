class ChartDimension
  include Mongoid::Document

  embedded_in   :chart

  field           :field_path,          type: String
  field           :label,               type: String

  @is_virtual = false

  def is_virtual=(value)
    @is_virtual = value
  end

  def is_virtual
    @is_virtual
  end

  def mongo_field_path; field_path.gsub('|', '.'); end

  #Creates virtual dimension with a specified index
  def self.virtual index
    c = ChartDimension.new(field_path: index)
    c.is_virtual = true
    c
  end
end
