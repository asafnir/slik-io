require 'pry'

class Chart 
  include Mongoid::Document
  rolify
  include Mongoid::Timestamps

  TYPES = {
    line: "Line",
    column: "Column",
    kpi: "KPI",
    pie: "Pie"
  }

  field     :name,                  type: String
  field     :chart_type,            type: String
  field     :public_id,             type: String, default: -> {"chart_#{SecureRandom.hex(10)}"}
  field     :title,                 type: String
  field     :is_draft,              type: Boolean
  field     :filters,               type: Array

  embeds_many   :dimensions,  class_name: 'ChartDimension'
  embeds_many   :values,      class_name: 'ChartValue'
  
  belongs_to    :user_collection
  belongs_to    :user

  accepts_nested_attributes_for :dimensions, :values

  after_initialize {
    self.public_id = "chart_#{SecureRandom.hex(8)}" if public_id.nil?
  }

  def resolve_array_unwinds field_path, unwind_array
    return unwind_array unless field_path

    col = user_collection

    pos = nil
    while pos = field_path.index(UserCollection::FIELD_SEPARATOR, (pos || 0) + 1)
      field_subpath = field_path[0..pos - 1]
      if col.data_fields[field_subpath] == 'Array'
        unwind_array << "$#{field_subpath}" unless unwind_array.include?("$#{field_subpath}")
        field_path = field_path[pos + 1..-1]
      end
    end
  end

  def get_aggregation_provider(collection)
    Providers::AggregationFrameworkProvider.new(user_collection)
  end

  def get_data options = {}
    raise "Data source not set for chart #{id}" unless user_collection

    merged_dimensions = dimensions ? dimensions.map { |x| {field_path: x.field_path, label: x.label } } : []
    merged_values = values.map { |x| {field_path: x.field_path, label: x.label, aggregation_function: x.aggregation_function || 'sum'} }
    merged_filters = (filters || []) + (options[:filters] || [])

    collection = options[:collection_public_id] ? UserCollection.find(options[:collection_public_id]) : user_collection

    series = [{
      "labels" => (merged_dimensions + merged_values).map{|x| x[:label]},
      "types" => (merged_dimensions + merged_values).map{|x| user_collection.field_type(x[:field_path]) },
      "data" => get_aggregation_provider(collection).aggregate(
        dimensions: merged_dimensions,
        values: merged_values,
        filters: merged_filters
      )
    }]

    type = chart_type || options[:chart_type] || autodetect_chart_type(merged_dimensions, merged_values).to_s
    chart_data = {
      'title' => options[:title] || title,
      'name' => options[:name] || name,
      'type' => options[:type] || type,
      'series' => series,
      'filters' => merged_filters,
      'dimensions' => merged_dimensions,
      'values' => merged_values,
      'public_id' => public_id,
      'collection_id' => options[:collection_public_id] || user_collection.public_id,
      'is_draft' => is_draft
    }
    chart_data
  end

  def autodetect_chart_type dimensions = nil, values = nil
    type = nil
    if dimensions.nil? || dimensions.length == 0
      type = :kpi if values && values.length == 1
    elsif dimensions.length == 1
      if values && values.length == 1
        type = user_collection.field_comparable?(dimensions[0][:field_path]) ? :line : :pie
      elsif values && values.length > 1
        type = :column
      end
    else
      # Multiple dimensions not supported yet
    end
    type
  end
end
