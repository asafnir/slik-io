require 'csv'

class Upload
  include Mongoid::Document

  field       :filename,           type: String
  field       :content_type,       type: String
  field       :size,               type: Integer
  field       :headers,            type: String

  field       :records_total,      type: Integer
  field       :records_processed,  type: Integer
  field       :process_state,      type: String
  field       :tmp_file_path,      type: String
  field       :error_message,      type: String

  belongs_to  :user_collection
  belongs_to  :user

  def process_json
    c = user_collection
    self.records_processed = 0
    self.process_state = "processing"


    data = JSON(File.read(tmp_file_path))
    buffer = []
    if data.is_a?(Array)
      data.each {|d|
        self.records_processed += 1
        buffer << d

        if records_processed % 100 == 0
          c.add_data_record(buffer, 'json')
          save!
          buffer = []
        end
      }
    else
      buffer = [data]
      self.records_processed = 1
    end
    c.add_data_record(buffer, 'json') if buffer.length > 0
    c.save!
    self.process_state = "done"
  rescue Exception => e
    self.process_state = "failure"
    self.error_message = e.message
    Rails.logger.error e.inspect
  ensure
    save!
  end

  def process_csv
    c = user_collection

    self.records_processed = 0
    self.process_state = "processing"

    headers = []
    index = 0
    items = []
    CSV.foreach(File.open(tmp_file_path)) do |row|
      if index == 0
        headers = row #Getting the CSV headers
      else
        h = {}
        headers.each_with_index { |e, i| h[e] = row[i] }
        items << h
      end
      index += 1

      self.records_processed += 1
      if records_processed % 100 == 0
        c.add_data_record(items, 'csv')
        save!
        items = []
      end
    end

    c.add_data_record(items, 'csv') if items.length > 0

    c.save!
    self.process_state = "done"
  rescue Exception => e
    self.process_state = "failure"
    self.error_message = e.message
    Rails.logger.error e.inspect
  ensure
    save!
  end
end