class User
  include Mongoid::Document
  rolify
  include Mongoid::Timestamps

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  validates_presence_of :email
  # validates_uniqueness_of :email, case_sensitive: false
  validates_presence_of :encrypted_password
  
  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  # API Authentication
  field :api_public_key,      type: String, default: -> {"pubkey_#{SecureRandom.hex(10)}"}
  field :api_private_key,     type: String, default: -> {"prvkey_#{SecureRandom.hex(10)}"}

  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable
  field :pushed_at_least_one_record, :type => Boolean

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String

  field :trial_ends_at,    type: Time,    default: 1.month.since

  # run 'rake db:mongoid:create_indexes' to create indexes
  index({ email: 1 }, { unique: true, background: false })
  index({ api_public_key: 1 }, {unique: true, background: true })
  index({ api_private_key: 1 }, {unique: true, background: true })
  field :name, :type => String
  field :first_name,         :type => String, :default => ""
  field :last_name,          :type => String, :default => ""
  validates_presence_of :name
  attr_accessible :role_ids, :as => :admin
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :created_at, :updated_at, :first_name, :last_name

  field :exceeded_free_tier, type: Boolean, default: false
  field :free_tier_limit_warning_sent, type: Boolean, default: false
  field :stripe_customer_id, type: String
  field :cc_mask, type: String
  field :last_payment_date, type: Time
  field :free_events, type: Integer, default: 0
  field :has_cc_attached, type: Boolean, default: false

  has_many :user_collections
  has_many :charts

  embeds_one :rewards, class_name: "UserRewards"
  after_initialize {build_rewards if rewards.nil?}

  accepts_nested_attributes_for :rewards

  def notify_about_pushed_data! method
    unless pushed_at_least_one_record
      Analytics.track(user_id: email, event: "user_pushed_data", properties: {method: method}) 
      self.pushed_at_least_one_record = true
      save
    end
  end

  def find_last_payment_date
    last_payment_date || created_at
  end

  def is_payment_required?
    lpd = find_last_payment_date

    lpd + 1.month < Time.now.utc
  end

  def get_tiers
    [
      {event_limits: 50000, cost: 0},
      {event_limits: 100000, cost: 40},
      {event_limits: 1000000, cost: 125},
      {event_limits: 5000000, cost: 250},
      {event_limits: 20000000, cost: 750},
      {event_limits: 100000000, cost: 2000},
      {event_limits: 1000000000, cost: 0}
    ]
  end

  def find_tier events_count
    tiers = get_tiers

    tiers.select{|e| e[:event_limits] > events_count }.first()
  end

  def validate_free_tier_fair_usage
    return false if has_cc_attached

    lpd = find_last_payment_date

    total = chargeable_events_count_since(lpd)
    tier = find_tier(total)
    Rails.logger.debug "Number of chargeable events: #{total}"
    Rails.logger.debug "Tier: #{tier}"

    if tier[:cost] > 0 && !exceeded_free_tier #If it is a paid tier (and we have no CC)
      self.exceeded_free_tier = true
      save!

      send_free_tier_limit_exceeded_warning
    end

    if !exceeded_free_tier && !free_tier_limit_warning_sent && total.to_f/tier[:event_limits] >= 0.8 #If the amount of currently chargeable events is higher than 80% of the limit.
      self.free_tier_limit_warning_sent = send_free_tier_limit_warning
      save!
    end
  end

  def calculate_payment_amount date_from=nil, date_to=nil
    tier = calculate_tier(date_from, date_to)
    Rails.logger.error("Could not determine the tier for the user #{_id}.") unless tier
    tier[:cost]
  end

  def calculate_tier(date_from, date_to)
    date_from = date_from || find_last_payment_date()
    #Assuming free events expire by the end of the billing period
    total = chargable_events_count_since(date_from, date_to)
    find_tier(total)
  end

  def has_charts?
    !Chart.where(user_id: id).first.nil?
  end

  def chargeable_events_count_since date_from, date_to = nil
    total = events_count_since(date_from, date_to)
    total -= free_events
    total
  end

  def events_count_since date_from, date_to = nil
    Rails.logger.debug "Collections count: #{user_collections.count}"
    user_collections.sum { |c|
      Rails.logger.debug("Checking collection #{c._id}")
      begin
        timestamp_clause = { '$gte' => date_from.mongoize }
        timestamp_clause['$lte'] = date_to.mongoize if date_to
        result = c.mongo_collection.aggregate({'$match' => { '___timestamp' => timestamp_clause}},
                    {'$group' => { '_id' => nil, 'count' => {'$sum' => 1} }}
        )
        result = result[0]
        r = result ? result["count"] : 0
        r
      rescue Moped::Errors::OperationFailure => e
        Rails.logger.error e
        0
      end
    }
  end

  def add_collection name
    c = user_collections.build public_id: "col_#{SecureRandom.hex(5)}", name: name
    c.save!
    c
  end

  def get_traffic_size
    user_collections.sum { |c| 
      begin
        Mongoid.default_session.command(collStats: c.public_id,scale: 1024)["storageSize"]
      rescue Moped::Errors::OperationFailure => e
        logger.error e
        0
      end
    }
  end

  def set_stripe_customer_card card, mask
    #Trying to minimize the number of requests
    if stripe_customer_id
      stripe_customer = Stripe::Customer.retrieve(stripe_customer_id)
      stripe_customer.card = card
      stripe_customer.save
    else
      create_stripe_customer(card)
    end
    self.has_cc_attached = true
    self.cc_mask = mask
    self.exceeded_free_tier = false #Removing the flag, since user is now allowed to access paid tiers
    self.last_payment_date = Time.now.utc unless self.last_payment_date #Discarding whatever events we had previously.
    save!
  end

  def create_stripe_customer card
    c = Stripe::Customer.create(
        card: card,
        email: email,
        description: "#{first_name} #{last_name}",
        plan: get_plan_name
    )
    self.stripe_customer_id = c.id
    c
  end

  def get_events_count
    user_collections.sum { |c| 
      begin
        Mongoid.default_session.command(collStats: c.public_id)["count"]
      rescue Moped::Errors::OperationFailure => e
        logger.error e
        0
      end
    }
  end

  def send_on_create_confirmation_instructions
    #send_devise_notification(:confirmation_instructions)
  end

  def confirmation_url
    Rails.application.routes.url_helpers.user_confirmation_url({confirmation_token:  confirmation_token})
  end

  def reset_password_url
    ensure_reset_password_token!
    Rails.application.routes.url_helpers.edit_user_password_url(reset_password_token: reset_password_token)
  end

  def send_reset_password_instructions
    Analytics.track(user_id: email, event: "reset_password", properties: {
      reset_password_url: reset_password_url
    })
    #send_devise_notification(:reset_password_instructions)
  end

  def identify_for_analytics
    Analytics.identify(
      user_id: email,
      traits: { email: email, created_at: created_at.to_i, first_name: first_name, last_name: last_name }
    )
  end

  def send_free_tier_limit_warning
    Rails.logger.info "Sending free tier limit warning to user: #{email}"
    Analytics.track(
      user_id: email, event: 'free_plan_limit',
      properties: { email: email }
    )
  end

  def send_free_tier_limit_exceeded_warning
    Rails.logger.info "Sending free tier limit exceeded warning to user: #{email}"
    Analytics.track(
        user_id: email, event: 'free_plan_limit_exceeded',
        properties: { email: email }
    )
  end

  def self.enforce_limit_on_free_tier_users
    User.where('$or' => [{free_tier_limit_warning_sent: false}, {exceeded_free_tier: false}]).where(has_cc_attached: false).each {|x|
      begin
        x.validate_free_tier_fair_usage
        Rails.logger.info("Checked user free tier limit (user=#{x._id}).")
      rescue Exception => e
        Rails.logger.error "An error occurred while checking free tier limit for #{x._id}"
        Rails.logger.error e
      end
    }
    Analytics.flush()
  end

  private
  def get_plan_name
    (/\+weekly.+?@slik.io/ =~ self.email).nil? ? 'default_payg' : 'testing_weekly'
  end
end

class UserRewards
  include Mongoid::Document

  embedded_in :user

  ## Social networks flags
  field :twitter_share,    type: Boolean, default: false
  field :facebook_share,   type: Boolean, default: false
  field :linkedin_share,   type: Boolean, default: false
  field :google_share,     type: Boolean, default: false

  def has_left
    !facebook_share || !twitter_share || !linkedin_share || !google_share
  end

  def reset
    update_attributes(twitter_share: false, facebook_share: false, linkedin_share: false, google_share: false)
  end
end
