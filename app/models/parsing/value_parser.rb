module Parsing
  class ValueParser
    def self.parse_date d
      return d.to_time.utc if d.is_a?(Date)
      return d.utc if d.is_a?(Time)
      return d.utc if d.is_a?(DateTime)

      return nil if d.to_i.to_s == d

      begin
        return Time.iso8601(d.to_s)
      rescue ArgumentError => e
      end
      
      begin
        return Date.iso8601(d.to_s).to_time
      rescue ArgumentError => e
      end
      nil
    end
  end
end