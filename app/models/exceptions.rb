module Exceptions
  class Error < StandardError
  end

  class InvalidFieldNameError < Error
    def initialize name; super("Invalid field name: #{name}"); end
  end
end