class UserCollection
  include Mongoid::Document
  rolify
  include Mongoid::Timestamps

  field :name, type: String
  # field :user_id, type: String
  field :public_id, type: String, default: -> {"col_#{SecureRandom.hex(10)}"}
  field :data_fields, type: Hash
  field :is_public, type: Boolean, default: false

  has_many    :charts
  has_many    :uploads
  belongs_to  :user

  # index({ name: 1, user_id: 1 }, { unique: true, background: true })

  def mongo_collection; Mongoid.default_session[public_id]; end

  def field_comparable? field_path
    raise "Unknown field path: #{field_path}" unless data_fields.has_key? field_path

    type = data_fields[field_path]
    type == 'Integer' || type == 'Float' || type =='Datetime'
  end

  def field_is_date? field_path
    raise "Unknown field path: #{field_path}" unless data_fields.has_key? field_path
    data_fields[field_path] == 'Datetime'
  end

  def field_type_string field_path; 
    raise "Unknown field path: #{field_path}" unless data_fields.has_key? field_path
    data_fields[field_path]
  end

  def field_type field_path; data_fields[field_path]; end

  def add_data_record content, method = nil
    unless data_fields
      self.data_fields = {}
      save!
    end

    user.notify_about_pushed_data!(method) if user

    items = content.is_a?(Array) ? content : [content]

    mongo_collection.insert(
      items.map {|item| process_content(item).merge(
          ___timestamp: Time.now, 
          ___record_id: "rec_#{SecureRandom.hex(10)}", 
        )
      }
    )

    save!
  end

  FIELD_SEPARATOR = '.'

  def process_content obj, prefix = ""
    obj.each { |k,v|
      validate_field_name(k)
      if v.is_a? Hash
        obj[k] = process_content(v, "#{prefix}#{k.to_s}#{FIELD_SEPARATOR}") 
      elsif v.is_a? Array
        field_path = "#{prefix}#{k.to_s}"
        obj[k] = flatten_array(v)

        data_fields[field_path] = "Array"
        obj[k].each {|item| 
          unless item.is_a?(String) || item.is_a?(Fixnum)
            process_content(item, "#{prefix}#{k.to_s}#{FIELD_SEPARATOR}")
          end
        }
      else
        field_path = "#{prefix}#{k.to_s}"
        d = nil
        if v.nil? || v.to_s == ''
          obj[k] = v
        elsif v.to_s =~ /^[\+\-]?\d+\.\d*$/
          data_fields[field_path] = "Float"
          obj[k] = v.to_f
        elsif d = Parsing::ValueParser.parse_date(v)
          data_fields[field_path] = "Datetime"
          obj[k] = d
        elsif v.to_s =~ /^[\+\-]?\d+$/
          data_fields[field_path] = "Integer" unless data_fields[field_path] == 'Float'
          obj[k] = v.to_i
        elsif
          data_fields[field_path] = "String"
          obj[k] = v
        end
      end
    }
    obj
  end

  def self.fill_with_random_data collection, count=1000
    count.times.each {
      collection.add_data_record(
        date: rand(100000).minutes.ago.to_date.to_time,
        int_val: rand(1000),
        float_val: (rand * 1000).round(2),
        str: %w(a b c a1 b334).sample
      )
    }
  end

  private
  def flatten_array a
    res = []
    a.each {|x| 
      res += (x.is_a?(Array) ? flatten_array(x) : [x]) 
    }
    res
  end

  def validate_field_name name
    raise Exceptions::InvalidFieldNameError.new(name) if %w(@ $ * % & # ! ^ . \\ / | . , [ ] { } < > = " ' - ; :).index(name[0])
  end
end
