class ChartFilter
  include Mongoid::Document

  embedded_in   :chart

  field           :field_path,               type: String
  field           :operator,                 type: String
  field           :value,                    type: String

  def mongo_field_path; field_path.gsub('|', '.'); end

  def self.compile raw, filter
    parts = /([a-zA-Z0-9_\.]*?)\ (==|>=|<=|>|<)\ ([^\ ]*?)\ *$/.match(raw)
    filter.field_path = parts[1]
    filter.operator = parts[2]

    value = parts[3]
    string_match = value.match(/^'(.*?)'$/)       # Remove the quotes if present
    value = string_match[1] if string_match
    filter.value = value
  end

end
