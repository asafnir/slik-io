module Api
  module V1
    class CollectionsController < Api::V1::APIController
      before_filter :authenticate_user_by_api_key_or_session

      def index
        collections = @user.user_collections.map {|c| c}
        if params[:with_public]
          collections += UserCollection.where(is_public: true).all
        end
        if params[:include_public_with_my_charts]
          collection_ids = Chart.where(user_id: current_user.id).map {|c| c.user_collection_id}
          collections += UserCollection.where(is_public: true)
            .in(_id: collection_ids)
            .ne(user_id: current_user.id)
            .all
        end
        render json: collections.map { |c| collection_to_json(c) }
      end

      def show
        return unless require_parameter :id

        col = @user.user_collections.any_of({id: params[:id]}, {public_id: params[:id]}).first
        render json: collection_to_json(col)
      end

      def create
        return unless require_parameter :name

        col = @user.user_collections.build(name: params[:name])
        col.save!

        render json: collection_to_json(col)
      end

      def destroy
        return unless require_parameter :id
        col = @user.user_collections.find(params[:id])
        user = current_user

        unless col && user && (col.user_id == user.id)
          render :unauthorized, json: {error: "Collection doesn't exist or doesn't belong to you"}
          return
        end

        col.destroy

        render json: { success: "Collection #{params[:id]} deleted" }
      end

      private
      def collection_to_json c
        records_count = 0
        begin
          stats = Mongoid.default_session.command(collStats: c.public_id, scale: 1024)
          records_count = stats["count"]
          size = stats["storageSize"]
        rescue Moped::Errors::OperationFailure => e
        end

        charts = c.charts.where(user_id: @user.id, is_draft: false).map {|chart| {
            public_id: chart.public_id, 
            name: chart.name
        } }

        {
          id: c.id,
          user_id: c.user_id,
          public_id: c.public_id,
          name: c.name,
          editable: c.user_id == @user.id,
          is_public: c.is_public == true,
          items_count: records_count,
          data_fields: c.data_fields,
          charts: charts,
          charts_count: charts.length,
          size: size
        }
      end
    end
  end
end