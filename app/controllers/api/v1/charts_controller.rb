class Api::V1::ChartsController < Api::V1::APIController
  before_filter :resolve_chart
  layout false

  def show
    chart = override_chart_parameters @chart.clone
    respond_to do |format|
      if params[:callback]
        format.js { render json: chart.get_data(params), callback: params[:callback]  }
      else
        format.json { render json: chart.get_data(params)}
      end
      format.html
    end
  end

  def edit
    respond_to do |format|
      if params[:callback]
        format.js { render json: @chart, callback: params[:callback] }
      else
        format.json { render json: @chart }
      end
      format.html
    end
  end

  def dynamic
    chart = build_chart_from_parameters
    respond_to do |format|
      if params[:callback]
        format.js { render json: chart.get_data(), callback: params[:callback]  }
      else
        format.json { render json: chart.get_data() }
      end
      format.html
    end
  end

  def destroy
    user = current_user
    unless @chart && user && (@chart.user_id == user.id || @chart.user_collection.user_id == user.id)
      render :unauthorized, json: {error: "Chart doesn't exist or doesn't belong to you"}
      return
    end

    @chart.destroy

    render json: {success: "Chart #{@chart.public_id} deleted"}
  end

  def create
    chart = build_chart_from_parameters
    chart.save!

    Analytics.track(user_id: current_user.email, event: "chart_created", properties: {
      chart_id: chart.public_id, 
      chart_type: chart.chart_type || "auto"
    })

    res = {success: true, id: chart.public_id, public_id: chart.public_id}
    respond_to do |format|
      if params[:callback]
        format.js { render json: res, callback: params[:callback]  }
      else
        format.json { render json: res }
      end
    end
  end

  def update
    chart = @chart
    
    override_chart_parameters(chart)
    chart.save!

    Analytics.track(user_id: current_user.email, event: "chart_edited", properties: {
      chart_id: chart.public_id, 
      chart_type: chart.chart_type || "auto"
    })

    res = {success: true, id: chart.public_id}
    respond_to do |format|
      if params[:callback]
        format.js { render json: res, callback: params[:callback]  }
      else
        format.json { render json: res }
      end
    end
  end

  def exists
    chart = Chart.where({name: params[:name]}).first
    res = { success: true, available: chart.nil? || chart.public_id == params[:id] }
    respond_to do |format|
      if params[:callback]
        format.js { render json: res , callback: params[:callback]  }
      else
        format.json { render json: res }
      end
    end
  end

  private
  def build_chart_from_parameters
    chart = Chart.new 
    override_chart_parameters(chart)
  end

  def override_chart_parameters chart
    if params[:collection_id]
      c_id = params[:collection_id] 
      collection = UserCollection.where({public_id: c_id}).first
      chart.user_collection = collection
    end
    if params[:dimensions]
      chart.dimensions.clear #WARNING: this shit automatically persists everything
      for x in params[:dimensions]
        x = x[1] if x[1] #In GET URL object arrays are hashes {"0" => {field_path: ...}}
        chart.dimensions.build field_path: x[:field_path], label: x[:label]
      end
    end
    if params[:values]
      chart.values.clear
      for x in params[:values]
        x = x[1] if x[1] #In GET URL object arrays are hashes {"0" => {field_path: ...}}
        p = {field_path: x[:field_path], label: x[:label]}
        p[:aggregation_function] = x[:aggregation_function] if x[:aggregation_function]
        chart.values.build p
      end
    end
    chart.filters = params[:filters].map{|k,v| v} if params[:filters]
    puts chart.filters.inspect
    chart.title = params[:title] unless params[:title].blank?
    chart.chart_type = params[:type] unless params[:type].blank?
    chart.name = params[:name] unless params[:name].blank?
    chart.is_draft = false if params[:is_draft] != true
    chart.user = current_user
    chart
  end
end