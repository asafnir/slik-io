module Api
  module V1
    class DataController < Api::V1::APIController
      before_filter :authenticate_user_by_api_key
      before_filter :resolve_collection!

      def index
        data = params[:data]

        return render json: {success: false, message: 'The account has exceeded the limit of events available in free tier. Please provide billing information.'} if @user.exceeded_free_tier

        @collection.add_data_record(JSON(data), 'api')
        render inline: '', layout: false
      end

      def create
        data = params[:data]

        return render json: {success: false, message: 'The account has exceeded the limit of events available in free tier. Please provide billing information.'} if @user.exceeded_free_tier

        @collection.add_data_record(data, 'api')
        render status: :ok, json: { success: "Data added to collection #{@collection.id}" }
      end
    end
  end
end