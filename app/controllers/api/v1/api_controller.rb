module Api
  module V1
    class APIController < ApplicationController
      def resolve_chart
        @chart_id = params[:chart_id] || params[:id]
        @chart = Chart.where(public_id: @chart_id).first
      end

      def resolve_collection
        @collection_id = params[:collection_id] || params[:id]
        @collection = UserCollection.where({public_id: @collection_id, user_id: @user.id}).first
      end

      def resolve_collection!
        resolve_collection

        render(status: :bad_request, json: {
          error: "Unknown collection #{@collection_id}"  
        }) and return unless @collection
      end

      def authenticate_user_by_api_key
        api_key = params[:api_public_key]
        authenticate_with_http_basic do |user, password|
          api_key = user 
        end unless api_key

        @user = User.any_of({api_public_key: api_key}, {api_private_key: api_key}).first

        render(status: :unauthorized, json: {
          error: "Not authorized. The API public/private key you provided is invalid."
        }) and return unless  @user
      end

      def authenticate_user_by_api_key_or_session
        @user = current_user
        authenticate_user_by_api_key unless @user        
      end

      def require_parameter param
        unless params[param]
          render status: :bad_request, json: {error: "Missing mandatory parameter '#{param.to_s}'"}
          return false
        end
        true
      end
    end
  end
end