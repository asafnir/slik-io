class BonusesController < ApplicationController
	before_filter :authenticate_user!

	def index
		respond_to do |format|
			format.html
			format.json {
				render json: {
					facebook: !current_user.rewards.facebook_share && (ENV["FACEBOOK_ENABLE_SHARE"]  == 'true'),
					twitter: !current_user.rewards.twitter_share && (ENV["TWITTER_ENABLE_SHARE"]  == 'true'),
					linkedin: !current_user.rewards.linkedin_share && (ENV["LINKEDIN_ENABLE_SHARE"]  == 'true'),
					google: !current_user.rewards.google_share && (ENV["GOOGLE_ENABLE_SHARE"] == 'true'),
					facebook_id: ENV["FACEBOOK_ID"],
          free_events: ENV['FREE_RECORDS_FOR_SHARE']
				}
			}
		end
	end

	def create
    attr_name = "#{params[:network]}_share"
    v = case params[:network]
          when 'facebook', 'twitter', 'linkedin', 'google'
            current_user.rewards.read_attribute(attr_name)
          else
            return render json: {success: false}
        end

    return render json: {success: true, free_events: current_user.free_events} if v #If the already shared - no bonus

    current_user.rewards.write_attribute(attr_name, true)
    current_user.free_events += ENV["FREE_RECORDS_FOR_SHARE"].to_i
    current_user.save!

		render json: {success: true, free_events: current_user.free_events}
	end
end