class CollectionsController < ApplicationController
  before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token, :only => [:load_csv] 
  
  def index
    respond_to do |format|
      format.html
      format.json { 
        q = params[:public] ? UserCollection.where(:public => true) : UserCollection.where(:user_id => current_user._id)
        q = q.where(:name => params[:name]) if params[:name]
        render json: q.map { |col| 
          {
            _id: col._id,
            name: col.name,
            charts: col.charts.where({is_draft: false}).map {|chart| {public_id: chart.public_id, name: chart.name} },
            public_id: col.public_id,
            data_fields: col.data_fields,
            user_id: col.user_id
          }
        }
      }
    end
  end

  def show
    respond_to do |format|
      format.json {
        render json: UserCollection.any_of(id: params[:id], public_id: params[:id]).first
      }
    end
  end

  def create
    c = UserCollection.new({
        :user_id => current_user._id,
        :public => false,
        :name => params[:name]
      })
    c.save!
    
    render json: { id: c._id, public_id: c.public_id, success: true}
  end
end