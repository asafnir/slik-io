class DeviseUsers::ConfirmationsController < Devise::ConfirmationsController
  respond_to :json

  def show
    respond_to do |format|
      format.html {
        @transition_to = 'activate_account'
        @transition_params = { confirmation_token: params[:confirmation_token] }

        render inline: '', layout: 'application'
      }
      format.json {
        self.resource = resource_class.confirm_by_token(params[:confirmation_token])
        if resource.errors.empty?
          Analytics.track(user_id: resource.email, event: "user_activated_account", properties: {email: resource.email})
          sign_in resource_name, resource
          puts "#{resource_name}, #{resource}"
          render json: {success: "Your account is activated" }
        else
          render json: {error: "Activation failed"}, status: :bad_request
        end        
      }
    end
    
  end

  def create
    user = User.where(email: params[:user][:email]).first
    if user
      user.send :generate_confirmation_token!
      Analytics.track(user_id: user.email, event: "user_requested_activation_resend", properties: {
        activation_url: user.confirmation_url
      })
    end
    render json: { success: "Activation instructions sent" }
  end
end