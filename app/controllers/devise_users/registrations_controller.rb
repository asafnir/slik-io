class DeviseUsers::RegistrationsController < Devise::RegistrationsController
  respond_to :json, :html

  def create
    #Leaving this part here, just in case we will need password confirmation back in the future
    #if params[:user][:password] != params[:user][:password_confirmation]
    #  render json: {error: 'Passwords do not match' }, status: 422
    #  return
    #end
    user = User.new(params[:user])
    user.name = user.email
    if user.save
      user.identify_for_analytics
      Analytics.track(user_id: user.email, event: "user_signed_up", properties: {
        activation_url: user_confirmation_url({confirmation_token: user.confirmation_token})
      })
      render json: {success: true}, status: 201
    else
      warden.custom_failure!
      render json: {error: user.errors }, status: 422
    end
  end

  def email_available
    render json: { success: true, available: User.where(email: params[:email]).count == 0 }
  end
end