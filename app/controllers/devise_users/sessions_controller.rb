class DeviseUsers::SessionsController < Devise::SessionsController  
  respond_to :json, :html

  def create  
    respond_to do |format|  
      format.html { super }  
      format.json {  
        warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#new")  
        render status: 200, json: { success: true }  
      }  
    end  
  end

  def destroy  
    super  
  end  

  def current
    cu = current_user
    render json: {
      success: true,
      _id: cu._id,
      api_public_key: cu.api_public_key,
      api_private_key: cu.api_private_key
    }
  end
end 