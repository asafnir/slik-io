class DeviseUsers::PasswordsController < Devise::PasswordsController
  respond_to :json, :html

  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)

    render json: {success: true}
  end

  def edit
    @transition_to = 'password_change'
    @transition_params = { reset_password_token: params[:reset_password_token] }

    render inline: '', layout: 'application'
  end

  def update
    #self.resource = resource_class.reset_password_by_token(resource_params)
    self.resource = resource_class.reset_password_by_token(params)

    if resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)
      #flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
      #set_flash_message(:notice, flash_message) if is_navigational_format?
      #sign_in(resource_name, resource)
      render json: {success: true}
    else
      render json: {success: false, errors: resource.errors}
    end
  end

end