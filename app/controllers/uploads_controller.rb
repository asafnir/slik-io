class UploadsController < ApplicationController
  before_filter       :authenticate_user!
  skip_before_filter  :verify_authenticity_token, :only => [:create] 

  def show
    upload = Upload.where(id: params[:id]).first

    unless upload
      render status: :bad_request, json: {error: "Unable to find upload #{params[:id]}"}
      return
    end

    render json: upload_as_json(upload)
  end

  # Parameters: {
  #  "file"=>#<ActionDispatch::Http::UploadedFile:0x007fe2bd3714a8 
  #     @original_filename="billing_transactions_trunc.csv", 
  #     @content_type="text/csv", 
  #     @headers="Content-Disposition: form-data; name=\"file\"; filename=\"billing_transactions_trunc.csv\"\r\nContent-Type: text/csv\r\n", 
  #     @tempfile=#<File:/var/folders/9y/59w4kfn96zqgmz7j_ln2ks5w0000gn/T/RackMultipart20130906-17155-1ejskwc>>, 
  #  "id"=>"col_7eab9a83f00fd70401bf"}
  def create
    col = UserCollection.where(public_id: params[:collection_id], user_id: current_user.id).first if params[:collection_id]

    unless col
      render status: :bad_request, json: {error: "Unable to find collection #{params[:collection_id]}"}
      return
    end

    file = params[:file]
    upload = col.uploads.create! filename: file.original_filename, content_type: file.content_type,
      tmp_file_path: file.tempfile.path, headers: file.headers, user_id: current_user.id


    if ['application/json'].index(file.content_type)
      Thread.new { upload.process_json } 
    elsif ['text/csv', 'text/comma-separated-values', 
        'application/excel', 'application/vnd.ms-excel', 
        'application/vnd.msexcel'].index(file.content_type)
      Thread.new { upload.process_csv }
    else
      raise "Unknown content type: #{file.content_type}"
    end

    render json: upload_as_json(upload)
  end

  private
  def upload_as_json upload
    {
      id: upload.id,
      total: upload.records_total,
      processed: upload.records_processed,
      error_message: upload.error_message,
      state: upload.process_state
    }
  end
end