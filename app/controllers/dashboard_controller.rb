class DashboardController < ApplicationController
  before_filter :authenticate_user!

	def index
    respond_to do |format|
      format.html { render inline: '', layout: 'application' }
      format.json {
        render json: {
          traffic_size: current_user.get_traffic_size,
          events_count: current_user.get_events_count
        }
      }
    end
	end
end