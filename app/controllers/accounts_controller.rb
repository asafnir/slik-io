class AccountsController < ApplicationController
  include ActionView::Helpers::DateHelper

  before_filter { |c|
    self.send(:authenticate_user!) unless params[:test]
  }

  def show
    respond_to do |format|
      format.js {
        if params[:test]
          render json: {logged_in: user_signed_in?, account: account_to_json(current_user)}
        else
          render json: account_to_json(current_user)
        end
      }
    end
  end

  def update
    current_user.first_name = params[:first_name] if params[:first_name]
    current_user.last_name = params[:last_name] if params[:last_name]
    current_user.save!

    current_user.identify_for_analytics

    respond_to do |format|
      format.js { render json: account_to_json(current_user) }
    end
  end

  def updateCC
    token = params[:token]

    current_user.set_stripe_customer_card token, params[:mask]

    respond_to do |format|
      format.js {
        render json: {success: true}
      }
    end
  end

  private

  def account_to_json account
    return nil unless account
    {
      id: account.id,
      email: account.email,
      api_public_key: account.api_public_key,
      api_private_key: account.api_private_key,
      first_name: account.first_name,
      last_name: account.last_name,
      has_cc_attached: account.has_cc_attached,
      cc_mask: account.cc_mask,
      trial_ends_in_words: distance_of_time_in_words(Time.now, account.trial_ends_at),
      is_in_trial: account.trial_ends_at > Time.now,
      can_get_rewards: account.rewards.has_left,
      traffic_size: account.get_traffic_size,
      records_number: account.get_events_count,
      has_charts: !account.charts.first.nil?
    }
  end
end
