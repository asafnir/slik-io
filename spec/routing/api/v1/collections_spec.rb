require 'spec_helper'

describe "/api/v1/collections routing" do
  it "routes POST /api/v1/collections/:collection_id/data to api/v1/data#create" do
    expect(
      post: '/api/v1/collections/col_123/data'
    ).to route_to(
      controller: 'api/v1/data',
      action: 'create',
      collection_id: 'col_123'
    )
  end

  it "routes GET /api/v1/collections/:id to api/v1/collections#index" do
    expect(
      get '/api/v1/collections'
    ).to route_to(
      controller: 'api/v1/collections',
      action: 'index'
    )
  end
end