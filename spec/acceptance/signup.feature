Feature: Sign up user
  Background:
    Given a visitor

  @javascript
  Scenario: Sign-up succeeds
    When I visit sign-up page
    And I enter my "first name" in firstName field
    And I enter my "last name" in lastName field
    And I enter my email in email field
    And I enter my password in password field
    And I check agreeToTerms
    And I click "Sign up" button
    Then I become a user
    Then I should not be logged in
    And I should see "Account created. You will receive an email with confirmation instructions shortly." text

    When I visit my confirmation_url
    Then I should see "Account activated successfully. You're now logged in." text
