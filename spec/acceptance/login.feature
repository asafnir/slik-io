Feature: Sign in user
  Background:
    Given a user

  @javascript
  Scenario: User with at least one chart
    Given I have a chart
    When I visit sign-in page
    And I enter my email in email field
    And I enter my password in password field
    And I click login
    Then I should be logged in
    And I should see the dashboard page

  @javascript
  Scenario: No-charts login
    When I visit sign-in page
    And I enter my email in email field
    And I enter my password in password field
    And I click login
    Then I should be logged in
    And I should see the new-chart page

  @javascript
  Scenario: Forgot password 
    # Recovery password step - instructions should be sent
    When I visit sign-in page
    And I click "Forgot your password?"
    Then I should see the recover-password page
    And I enter my email in email field
    And I click "reset password"
    Then I should see "Password recovery instructions have been sent to your email." text

    # Reset password step - new password is entered
    When I visit my reset_password_url
    And I choose my new password
    And I enter my password in password field
    And I click "Change"
    Then I should see "The password has been successfully changed." text

    Then I visit sign-in page
    And I enter my email in email field
    And I enter my password in password field
    And I click login
    Then I should be logged in
    And I should see the 'new-chart' page
