module CommonSteps
  PAGE_URLS = {
    'sign-in' => '/#/users/sign_in',
    'sign-up' => '/#/users/sign_up',
    'dashboard' => '/#/dashboard',
    'recover-password' => '/#/users/recover_password',
    'new-chart' => '/#/charts/new'
  }

  step "a visitor" do
    pwd = SecureRandom.hex(5)
    @user = FactoryGirl.build :user, email: "#{SecureRandom.hex(5)}@slik.io", password: pwd, password_confirmation: pwd
  end

  step "a user" do
    @user = FactoryGirl.create :user
  end

  step "I have a chart" do
    @chart = @user.charts.build
    @chart.save!
  end

  step "I choose my new password" do
    @user.password = SecureRandom.hex(5)
  end

  step "I enter my :attr_name in :field_name field" do |attr_name, field_name|
    fill_in field_name, with: @user.send(attr_name.gsub(' ', '_'))
  end

  step "I become a user" do
    timeout = 5.seconds.from_now
    while @user.nil? && Time.now < timeout do
      @user = User.where(email: @user.email).first
      @user.should_not be_nil
      sleep 0.1
    end

  end

  step "I visit my :url_attr" do |url_attr|
    url = @user.send(url_attr).gsub(/https?\:\/\/[^\/\:]*(\:\d+)?/, "")
    puts "Navigating to #{url}"
    visit url
  end

  step "I visit :name page" do |name|
    url = PAGE_URLS[name]

    raise "Unknown page #{name}" unless url

    puts "Navigating to #{url}"
    visit(url)
  end

  step "I check :name" do |name|
    check name
  end

  step "I click :name" do |name|
    click_on name
    puts "#{name} clicked"
  end

  step "I click :button button" do |button|
    click_button button
    puts "Button #{button} clicked"
  end

  step "I should be logged in" do
    
  end

  step "I should not be logged in" do

  end

  step "I should see :text text" do |text|
    page.should have_content(text)
  end

  step "I should see the :name page" do |name|
    puts "Checking if at #{name} (#{PAGE_URLS[name]}) page"
    timeout = 5.seconds.from_now
    while !page.current_url.end_with?(PAGE_URLS[name]) && Time.now < timeout do
      sleep 0.1
    end
    puts "Current url: #{page.current_url}"
    page.current_url.should end_with(PAGE_URLS[name])
  end
end