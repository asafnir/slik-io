module AuthenticateUser
  def set_auth_headers
    raise Exception.new "'publisher' is not defined" unless defined? :user

    request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Basic.encode_credentials(
      user.api_private_key, ""
    ) 
  end
end