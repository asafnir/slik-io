require 'spec_helper'

describe Api::V1::APIController do
  before :all do
    class ::TestingController < Api::V1::APIController
      before_filter :authenticate_user_by_api_key, only: [:needs_user, :needs_collection]
      before_filter :resolve_collection
      before_filter :resolve_collection!, only: [:needs_collection]

      def needs_user
        render nothing: true, status: 200
      end

      def needs_collection
        render nothing: true, status: 200
      end
    end

    Visually::Application.routes.draw do
      match 'success', :to => 'testing#success'
      match 'needs_user', :to => 'testing#needs_user'
      match 'needs_collection', :to => 'testing#needs_collection'
    end
  end
  after :all do Rails.application.reload_routes! end
  before :each do @controller = TestingController.new end


  let (:user) {FactoryGirl.create :user}

  describe 'HTTP Authentication' do
    context "invalid authentication" do
      it 'returns 400 when invalid api key specified' do
        request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Basic.encode_credentials(
          UUID.new.generate, ""
        ) 

        get :needs_user
        assigns(:user).should be_nil
        response.status.should == 401
        JSON(response.body)['error'].should =~ /Not authorized/
      end
    end

    context "Valid authentication" do
      after :each do
        get :needs_user
        response.status.should == 200
        assigns(:user).should_not be_nil
      end

      it 'authenticates by api_public_key' do
        request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Basic.encode_credentials(
          user.api_public_key, ""
        ) 
      end

      it 'authenticates by api_private_key' do
        request.env["HTTP_AUTHORIZATION"] = ActionController::HttpAuthentication::Basic.encode_credentials(
          user.api_private_key, ""
        ) 
      end
    end
  end

  let (:collection) { user.add_collection name: SecureRandom.hex(5) }

  describe 'resolve_collection' do
    before :each do set_auth_headers end
    it 'resolves collection for valid collection_id' do
      get :needs_collection, {collection_id: collection.public_id}
      assigns(:collection_id).should == collection.public_id
      assigns(:collection).public_id == collection.public_id
    end

    it 'sets collection = nil for invalid collection_id' do
      get :needs_collection
      assigns(:collection_id).should be_nil
      assigns(:collection).should be_nil
    end
  end

  describe 'resolve_collection!' do
    before :each do set_auth_headers end
      it "returns bad_request for invalid collection_id" do
        get :needs_collection, {collection_id: "123123123"}
        assigns(:collection).should == nil
        response.status.should == 400
        JSON(response.body)['error'].should =~ /Unknown collection/
      end
  end
end
