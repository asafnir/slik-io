require 'spec_helper'

describe Api::V1::DataController do
  let (:user) {FactoryGirl.create :user}
  let (:collection) {user.add_collection name: "Some name"}

  before :each do set_auth_headers end

  describe 'POST /api/v1/collections/:col_id/data' do
    let (:unique_value) { SecureRandom.hex(10) }
    let (:data) { {
      my_value: unique_value, 
      obj1: { int_val: 123, date: '12/15/2011' },
      obj2: { float_val: 123, date: '15/12/2013' },
      date: '15-10-2012',
      date1: '20121015'
    } }

    it 'adds the supplied record to the collection' do
      post :create, {data: data, collection_id: collection.public_id}
      JSON(response.body)['success'].should =~ /Data added/
      response.status.should == 200

      obj = collection.mongo_collection.find('my_value' => unique_value).first
      obj.should_not be_nil
    end

    it 'adds multiple records to the collection if array is passed' do
      data = [
        {val: SecureRandom.hex(5)},
        {val: SecureRandom.hex(5)}
      ]
      post :create, {data: data, collection_id: collection.public_id}

      collection.mongo_collection.find('val' => data[0][:val]).first.should_not be_nil
      collection.mongo_collection.find('val' => data[1][:val]).first.should_not be_nil
    end
  end
end