require 'spec_helper'

describe Api::V1::ChartsController do
  let (:user) {FactoryGirl.create :user}
  let (:collection) {FactoryGirl.create :user_collection, user_id: user.id}

  before :each do sign_in(user) end

  describe "create chart" do
    it 'creates exactly one chart' do
      post :create, {collection_id: collection.id}
      user.charts.length.should == 1
    end

    it 'associates the chart with the user' do
      post :create, {collection_id: collection.id}
      user.charts.first.user_id.should == user.id
    end
  end
end