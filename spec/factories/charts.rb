# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :chart do
    name { "Chart #{SecureRandom.hex(5)} " }
    user_collection { create :user_collection }
    user { user_collection.user }
  end
end
