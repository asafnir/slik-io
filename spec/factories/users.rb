# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name 'Test User'
    email { "example#{SecureRandom.hex(5)}@example.com" }
    password 'changeme'
    password_confirmation 'changeme'
    api_public_key { SecureRandom.hex(5) }
    api_private_key { SecureRandom.hex(5) }
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
  end
end
