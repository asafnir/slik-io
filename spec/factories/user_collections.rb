# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_collection do
    user {create :user}
    public_id {SecureRandom.hex(5)}
  end
end
