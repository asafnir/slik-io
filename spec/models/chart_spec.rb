require 'spec_helper'

describe Chart do
  let (:chart) { FactoryGirl.create :chart }

  describe '#autodetect_chart_type' do
    before :each do
      chart.user_collection.add_data_record date: Time.now, cost: 10, type: 'subscription'
    end

    context 'nil' do
      it 'returns nil when chart data not set' do
        chart.autodetect_chart_type.should be_nil
      end
    end

    context 'kpi' do
      it 'detects :kpi' do
        chart.autodetect_chart_type(nil, [{aggregation_function: :sum, field_path: 'cost'}]).should == :kpi
      end
    end

    context 'line' do
      it 'detects :line' do
        chart.autodetect_chart_type(
          [{field_path: 'date'}], 
          [{aggregation_function: :sum, field_path: 'cost'}]
        ).should == :line
      end
    end

    context 'pie' do
      it 'detects :pie' do
        chart.autodetect_chart_type(
          [{field_path: 'type'}],
          [{aggregation_function: :sum, field_path: 'cost'}]
        ).should == :pie
      end
    end
  end

  describe '#get_data' do
    _1_day_ago = 1.day.ago.utc.round(0)
    _3_days_ago = 3.days.ago.utc.round(0)
    _5_days_ago = 5.days.ago.utc.round(0)
    before :each do
      chart.user_collection.add_data_record date: _1_day_ago, cost: 10, type: 'subscription', operations: [
        { authorized: 10, date: _3_days_ago },
        { captured: 10, date: _1_day_ago }
      ]
      chart.user_collection.add_data_record date: _1_day_ago, cost: 20, type: 'one-time', operations: [
        { authorized: 20, date: _5_days_ago },
        { captured: 10, date: _3_days_ago }
      ]
      chart.user_collection.add_data_record date: _5_days_ago, cost: 5, type: 'subscription'
      chart.user_collection.add_data_record date: _5_days_ago, cost: 150, type: 'one-time'

      chart.user_collection.add_data_record date: _3_days_ago, cost: 50, type: 'subscription'
    end

    examples = [
      # {
      #   title: 'Multivalue chart',
      #   dimensions: [{field_path: 'date', label: 'Date'}],
      #   values: [{field_path: 'cost', label: 'cost'}, {field_path: 'operations|authorized', label: 'auth'}],
      #   expected_result: {
      #     'type' => 'column',
      #     'series' => [
      #       {
      #         'labels' => ['Date', 'cost', 'auth'],
      #         'types' => ['Datetime', 'Integer', 'Integer'],
      #         'data' => [[_5_days_ago.to_i, 155, 0], [_3_days_ago.to_i, 50, 0], [_1_day_ago.to_i, 30, 30]]
      #       }
      #     ]
      #   }
      # },
      {
        title: "Nested array with objects chart",
        dimensions: [{field_path: 'operations.date', label: "Date"}],
        values: [{field_path: 'operations.captured', aggregation_function: :sum, label: 'Captured amount'}],
        expected_result: {
          'title' => 'Nested array with objects chart',
          'type' => 'line',
          'series' => [
            {
              'labels' => ['Date', 'Captured amount'],
              'types' => ['Datetime', 'Integer'],
              'data' => [[_5_days_ago, 0], [_3_days_ago, 10], [_1_day_ago, 10]]
            }
          ]
        }
      },
      {
        title: 'Sum chart',
        chart_type: :column,
        dimensions: [{field_path: 'date', label: 'Date'}], 
        values: [{field_path: 'cost', aggregation_function: :sum, label: 'Total cost'}],
        expected_result: {
          'title' => 'Sum chart',
          'type' => 'column',
          'series' => [
            {
              'labels' => ['Date', 'Total cost'],
              'types' => ['Datetime', 'Integer'],
              'data' => [[_5_days_ago, 155], [_3_days_ago, 50], [_1_day_ago, 30]]
            }
          ]
        }
      },
      {
        title: 'Average chart',
        dimensions: [{field_path: 'type', label: 'Type'}],
        values: [{field_path: 'cost', aggregation_function: :avg, label: 'Avg cost'}],
        expected_result: {
          'title' => 'Average chart',
          'type' => 'pie',
          'series' => [
            {
              'labels' => ['Type', 'Avg cost'],
              'types' => ['String', 'Integer'],
              'data' => [['one-time', 85], ['subscription', (65.0/3)]]
            }
          ]
        }
      },
      {
        title: 'Max() chart',
        dimensions: [{field_path: 'date', label: 'Date'}],
        values: [{field_path: 'cost', aggregation_function: :max, label: 'Max cost'}],
        expected_result:  {
          'title' => "Max() chart",
          'type' => 'line',
          'series' => [
            'labels' => ['Date', 'Max cost'],
            'types' => ['Datetime', 'Integer'],
            'data' => [[_5_days_ago, 150], [_3_days_ago, 50], [_1_day_ago, 20]]
          ]
        }
      },
      {
        title: 'Sum KPI',
        values: [{field_path: 'cost', aggregation_function: :sum, label: 'Sum cost'}],
        expected_result: {
          'title' => 'Sum KPI',
          'type' => 'kpi',
          'series' => [
            'labels' => ['Sum cost'],
            'types' => ['Integer'],
            'data' => [[nil, 235]]
          ]
        }
      }
    ].each {|e|
      it "#{e[:title]}" do
        chart.title = e[:title]
        chart.chart_type = e[:chart_type]
        
        chart.dimensions.clear
        chart.values.clear

        e[:dimensions].each {|d| chart.dimensions.build(d)} if e[:dimensions]
        e[:values].each {|v| chart.values.build(v)}

        chart_data = chart.get_data

        chart_data.should include(e[:expected_result])
      end
    }
  end
end
