describe Providers::AggregationFrameworkProvider do
  class Integer
    def days_ago_iso
      self.days.ago.utc.iso8601
    end

    def days_ago_int
      self.days.ago.utc.to_int
    end
  end

  def generate_data
    tx_types = ['subscription', 'one-time', 'recurring']
    emails = ['john@slik.io.test', 'dan@slik.io.test', 'fred@slik.io.test', 'jack@slik.io.test', 'paul@slik.io.test']
    order_items = ["Seat", "Add-on 1", "Add-on 2"]
    plans = [
      {name: 'Pro', cost: 100 },
      {name: 'Basic', cost: 15 },
      {name: 'Free', cost: 0 }
    ]
    (1..100).map { |index|
      {
        email: emails.sample,
        type: tx_types.sample,
        id: index,
        date: rand(15).days_ago_iso,
        amount: (rand * 200).round(2),
        plan: plans.sample,
        order: {
          items: (1..rand(5)).map {
            { code: "I#{SecureRandom.hex(5)}", name: order_items.sample, cost: (rand * 10).round(2) }
          }
        }
      }
    }
  end

  before :all do
    @user = FactoryGirl.create :user
    @collection = FactoryGirl.create :user_collection, user: @user

    @transactions = generate_data
    @collection.add_data_record @transactions

    @provider = Providers::AggregationFrameworkProvider.new(@collection)
  end

  describe 'aggregate' do
    it "handles multiple vlaues" do
      @provider.aggregate(
        values: [{aggregation_function: :sum, field_path: 'order.items.cost'}, {aggregation_function: :cnt, field_path: 'order.items.code'}],
      ).should == [
        [nil, *@transactions
          .flat_map{|tx| tx[:order][:items]}
          .inject([0, 0]) {|acc, x| [acc[0] + x[:cost], acc[1] + 1]}
        ]
      ]
    end

    it "handles filters" do
      @provider.aggregate(
        values: [{aggregation_function: :cnt, field_path: 'order.items.code'}],
        filters: ["order.items.name == 'Seat'", "plan.cost >= 15", "date > '#{5.days_ago_iso}'", "plan.cost != 15"]
      ).should == [
        [nil, @transactions
          .select{|tx| tx[:plan][:cost] > 15 && tx[:plan][:cost] != 15 && tx[:date] > 5.days.ago}
          .flat_map{|tx| tx[:order][:items]}
          .select{|tx| tx[:name] == 'Seat'}
          .length
        ]
      ]
    end

    it "computes total number of ordered items (support for nested fields)" do
      @provider.aggregate(
        values: [{ aggregation_function: :cnt, field_path: 'order.items.code'}]
      ).should == [[nil, @transactions.flat_map{|tx| tx[:order][:items]}.length]]
    end

    it "computes sum(amount)" do
      expected = @transactions.inject(0) {|aggr, x| aggr + x[:amount]}
      @provider.aggregate(
        values: [{ aggregation_function: :sum, field_path: 'amount' }]
      ).should == [[nil, expected]]
    end

    it 'computes avg(amount)' do
      total = @transactions.inject(0) {|aggr, x| aggr + x[:amount]}
      @provider.aggregate(
        values: [{ aggregation_function: :avg, field_path: 'amount' }]
      ).should == [[nil, (total / @transactions.length)]]
    end

    it 'computes max(amount)' do
      @provider.aggregate(
        values: [{ aggregation_function: :max, field_path: 'amount' }]
      ).should == [[nil, @transactions.map{|x| x[:amount]}.max]]
    end

    it 'computes min(amount)' do
      @provider.aggregate(
        values: [{ aggregation_function: :min, field_path: 'amount' }]
      ).should == [[nil, @transactions.map{|x| x[:amount]}.min]]
    end

    it 'computes cnt(amount)' do
      @provider.aggregate(
        values: [{ aggregation_function: :cnt, field_path: 'amount' }]
      ).should == [[nil, @transactions.length]]
    end

    it 'computes sum(amount) by date' do
      expected = @transactions
        .group_by{|x| x[:date]}
        .map{|k,g| [k, g.inject(0){|acc, item| acc + item[:amount]}]}
        .sort_by{|x| x[0]}
      @provider.aggregate(
        values: [{ aggregation_function: :sum, field_path: 'amount' }],
        dimensions: [{ field_path: 'date' }]
      ).should == expected
    end
  end
end