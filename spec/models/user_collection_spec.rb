require 'spec_helper'

describe UserCollection do
  let (:user) {FactoryGirl.create :user}
  let (:collection) {user.add_collection name: "Some collection"}

  describe 'add_data_record' do
    it "creates a record in the corresponding collection" do
      data = {a: SecureRandom.hex(10)}
      collection.add_data_record data
      collection.mongo_collection.find('a' => data[:a]).first.should_not be_nil
    end

    it "data_fields is persisted" do
      collection.add_data_record a: 1, d: 2
      collection.reload.data_fields.length.should == 2
    end

    %w(! @ # $ % ^ & * | { } [ ] = / \\ . , < > " ' ; : -).each { |c|
      it "doesn't accept fields starting with #{c}" do
        expect { collection.add_data_record({"#{c}a" => 1}) }.to raise_error(Exceptions::InvalidFieldNameError)
      end
    }


    [
      {
        data: {my_value: SecureRandom.hex(10)},
        fields: {"my_value" => "String"}
      },
      {
        data: {obj1: {int_val: 123, date: '2011-12-15' }},
        fields: {"obj1.int_val" => "Integer", "obj1.date" => "Datetime"}
      },
      {
        data: {obj2: {float_val: 123.123, date: '2013-12-15' }},
        fields: {"obj2.float_val" => "Float", "obj2.date" => "Datetime"}
      },
      {
        data: {int_val: "20130533"},
        fields: {"int_val" => "Integer"}
      },
      {
        data: {arr_val: [1,2,"1"]},
        fields: {"arr_val" => "Array"}
      },
      {
        data: {empty_val: '', nil_val: nil},
        fields: {}
      },
      {
        data: { obj_arr: [{date: '2011-12-15', val: 33.0}, {type: 'Some type', val: 20}] },
        fields: {"obj_arr" => "Array", "obj_arr.date" => "Datetime", "obj_arr.val" => "Float", "obj_arr.type" => "String"}
      },
      {
        data: {obj_with_nested_arr: { arr: [[2,3,4], "assss"] }},
        fields: {"obj_with_nested_arr.arr" => "Array"}
      },
      {
        data: { nested_arr: [ [[1,2,3], "asd"], "attr" ]},
        fields: {"nested_arr" => "Array"}
      }
    ].each { |test|
      it "populates collection#fields #{test[:data]}" do
        collection.add_data_record test[:data]

        collection.data_fields.should_not be_nil
        collection.data_fields.length.should == test[:fields].length
      end

      it "recognizes types for #{test[:data]}" do
        collection.add_data_record test[:data]
        collection.data_fields.should include(test[:fields])
      end
    }
  end

  describe "try_parse_date" do
    let (:examples) {{
      "2010-09-06T12:27:00.10+03:00" => "Valid",
      "2013-09-07" => "Valid",
      "01/01/2010" => nil,
      "31/01/2010" => nil,
      "06/06/1997" => nil,
      "20100303" => nil,
      "5 Jun 2000" => nil,
      "20130533" => nil
    }}
    it "recognizes all formats" do
      examples.each {|k,v|
        d = Parsing::ValueParser.parse_date(k)
        d.nil?.should == v.nil?
      }
    end
  end
end
